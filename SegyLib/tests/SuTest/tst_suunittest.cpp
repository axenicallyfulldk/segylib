#include <QtTest>

#include "types.h"
#include "sufile.h"

#define VERYSMALL  (1.0E-150)
#define EPSILON    (1.0E-6)
bool isEqual(double a, double b)
{
    double absDiff = fabs(a - b);
    if (absDiff < VERYSMALL)
    {
        return true;
    }

    double aAbs=std::fabs(a);
    double bAbs=std::fabs(b);
    double maxAbs  = aAbs>bAbs?aAbs:bAbs;
    return (absDiff/maxAbs) < EPSILON;
}

class SuUnitTest : public QObject
{
    Q_OBJECT

public:
    SuUnitTest();
    ~SuUnitTest();

private slots:
    void test_case1();
    void test_case2();
    void test_case3();

private:
    std::string suFilePath;
    size_t sampleCount;
    size_t traceCount;
    int sampleInterval;
    std::vector<std::vector<double> > traces;

    std::vector<long long> overridenTraceIndexes;
    std::vector<std::vector<double> > overridenTraces;
};

SuUnitTest::SuUnitTest()
{
    sampleCount = 100;
    traceCount = 1000;
    sampleInterval = 2000;
    suFilePath = "test.su";

    std::shared_ptr<SuFile<double> > sf(SuFile<double>::createSu(suFilePath, sampleCount, Format::FORMAT_IBM, Endian::ENDIAN_BIG));

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(-1000.0, 1000.0);

    for(size_t i=0;i<traceCount;++i){
        std::vector<double> trace;
        for(size_t j=0;j<sampleCount;++j){
            trace.push_back(dist(mt));
        }
        sf->setTrace(i,trace);
        traces.push_back(trace);
    }

    std::uniform_int_distribution<long long> traceDist(0, sf->getTraceCount()-1);
    for(size_t i = 0; i < 10; ++i){
        std::vector<double> trace;
        for(size_t j=0;j<sampleCount;++j){
            trace.push_back(dist(mt));
        }
        overridenTraces.push_back(trace);
        overridenTraceIndexes.push_back(traceDist(mt));
    }
}

SuUnitTest::~SuUnitTest()
{
    std::remove(suFilePath.data());
}

void SuUnitTest::test_case1()
{
    SuFile<double> sf(suFilePath, sampleCount, "r", SuTraceHeader::getDefaultMapping(), Format::FORMAT_IBM, Endian::ENDIAN_BIG);

    QCOMPARE(traceCount, sf.getTraceCount());
}

void SuUnitTest::test_case2()
{
    SuFile<double> sf(suFilePath, sampleCount, "r", SuTraceHeader::getDefaultMapping(), Format::FORMAT_IBM, Endian::ENDIAN_BIG);

    for(size_t i=0;i<traceCount;++i){
        bool res=true;
        auto sftrace=sf.getTrace(i);
        auto trace=traces[i];
        for(size_t j=0;j<trace.size();++j){
            res=res & isEqual(trace[j],sftrace->at(j));
        }
        QCOMPARE(res, true);
    }
}

void SuUnitTest::test_case3()
{
    SuFile<double> sf(suFilePath, sampleCount, "rw", SuTraceHeader::getDefaultMapping(), Format::FORMAT_IBM, Endian::ENDIAN_BIG);

    for(size_t i = 0; i < overridenTraceIndexes.size(); ++i){
        sf.setTrace(overridenTraceIndexes[i], overridenTraces[i]);
    }
    sf.closeFile();

    SuFile<double> sf2(suFilePath, sampleCount, "r", SuTraceHeader::getDefaultMapping(), Format::FORMAT_IBM, Endian::ENDIAN_BIG);

    for(size_t i = 0; i < overridenTraceIndexes.size(); ++i){
        bool res=true;
        auto sftrace=sf2.getTrace(overridenTraceIndexes[i]);
        auto trace= overridenTraces[i];
        for(size_t j=0;j<trace.size();++j){
            res=res & isEqual(trace[j],sftrace->at(j));
        }
        QCOMPARE(res, true);
    }
}

QTEST_APPLESS_MAIN(SuUnitTest)

#include "tst_suunittest.moc"
