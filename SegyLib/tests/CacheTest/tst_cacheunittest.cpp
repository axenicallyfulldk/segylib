#include <QtTest>

#include "icache.h"
#include "lrucache.h"

// add necessary includes here

class CacheUnitTest : public QObject
{
    Q_OBJECT

public:
    CacheUnitTest();
    ~CacheUnitTest();

private slots:
    void test_case1();

};

CacheUnitTest::CacheUnitTest()
{

}

CacheUnitTest::~CacheUnitTest()
{

}

template<class Key, class Value>
class CacheObserver : public ICacheReplacementObserver{
public:
    std::list<Key> keys;
    std::list<typename ICache<Key,Value>::ValuePtr> vals;
    ICache<Key, Value>* _cache;

    CacheObserver(ICache<Key, Value>* cache):_cache(cache){
        _cache->addRepObserver(this);
    }

    void handleReplacement(){
        for(auto it = _cache->repBegin(); it != _cache->repEnd(); ++it){
            keys.push_back(it.getKey());
            vals.push_back(it.getValue());
        }
    }
};

void CacheUnitTest::test_case1()
{
    LruCache<int, int> cache;
    cache.setMaxSize(100);

    CacheObserver<int, int> observer(&cache);

    for(int i = 0; i < 1000; ++i){
        cache.put(i, std::make_shared<int>(i));
        auto keyIt = observer.keys.rbegin();
        auto valIt = observer.vals.rbegin();
        for(int j  = i - 100; j >= 0; --j){
            QCOMPARE(*keyIt, j);
            QCOMPARE(**valIt, j);
            ++keyIt;
            ++valIt;
        }
    }
}

QTEST_APPLESS_MAIN(CacheUnitTest)

#include "tst_cacheunittest.moc"
