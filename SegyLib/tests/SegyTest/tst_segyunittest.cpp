#include <QtTest>

#include "segyheader.h"
#include "types.h"
#include "segyfile.h"

#define VERYSMALL  (1.0E-150)
#define EPSILON    (1.0E-6)
bool isEqual(double a, double b)
{
    double absDiff = fabs(a - b);
    if (absDiff < VERYSMALL)
    {
        return true;
    }

    double aAbs=std::fabs(a);
    double bAbs=std::fabs(b);
    double maxAbs  = aAbs>bAbs?aAbs:bAbs;
    return (absDiff/maxAbs) < EPSILON;
}

class SegyUnitTest : public QObject
{
    Q_OBJECT

public:
    SegyUnitTest();
    ~SegyUnitTest();

private slots:
    void test_case1();
    void test_case2();
    void test_case3();

private:
    std::string segyFilePath;
    size_t sampleCount;
    size_t traceCount;
    int sampleInterval;
    std::vector<std::vector<double> > traces;

    std::vector<long long> overridenTraceIndexes;
    std::vector<std::vector<double> > overridenTraces;
};

SegyUnitTest::SegyUnitTest()
{
    sampleCount = 100;
    traceCount = 1000;
    sampleInterval = 2000;
    segyFilePath = "test.sgy";

    SegyHeader sh;
    sh.set(SegyHeader::sampleInterval, sampleInterval);
    sh.set(SegyHeader::sampleCount,static_cast<int>(sampleCount));
    sh.set(SegyHeader::type,static_cast<int>(Format::FORMAT_IBM));
    std::shared_ptr<SegyFile<double> > sf(SegyFile<double>::createSegy(segyFilePath, sh, Endian::ENDIAN_BIG));

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(-1000.0, 1000.0);

    for(size_t i=0;i<traceCount;++i){
        std::vector<double> trace;
        for(size_t j=0;j<sampleCount;++j){
            trace.push_back(dist(mt));
        }
        sf->setTrace(i,trace);
        traces.push_back(trace);
    }

    std::uniform_int_distribution<long long> traceDist(0, sf->getTraceCount()-1);
    for(size_t i = 0; i < 10; ++i){
        std::vector<double> trace;
        for(size_t j=0;j<sampleCount;++j){
            trace.push_back(dist(mt));
        }
        overridenTraces.push_back(trace);
        overridenTraceIndexes.push_back(traceDist(mt));
    }
}

SegyUnitTest::~SegyUnitTest()
{
    std::remove(segyFilePath.data());
}

void SegyUnitTest::test_case1()
{
    SegyFile<double> sf2(segyFilePath, "r", SegyHeader::getDefaultMapping(),
                         SegyTraceHeader::getDefaultMapping(), Endian::ENDIAN_BIG);

    QCOMPARE(sf2.getTraceCount(), traceCount);
    QCOMPARE(sf2.getSampleCount(), sampleCount);
    QCOMPARE(sf2.getHeader().get(SegyHeader::sampleInterval), 2000);
}

void SegyUnitTest::test_case2()
{
    SegyFile<double> sf(segyFilePath, "r", SegyHeader::getDefaultMapping(),
                         SegyTraceHeader::getDefaultMapping(), Endian::ENDIAN_BIG);

    for(size_t i=0;i<traceCount;++i){
        bool res=true;
        auto sftrace=sf.getTrace(i);
        auto trace=traces[i];
        for(size_t j=0;j<trace.size();++j){
            res=res & isEqual(trace[j],sftrace->at(j));
        }
        QCOMPARE(res, true);
    }
}

void SegyUnitTest::test_case3()
{
    SegyFile<double> sf(segyFilePath, "rw", SegyHeader::getDefaultMapping(),
                         SegyTraceHeader::getDefaultMapping(), Endian::ENDIAN_BIG);

    for(size_t i = 0; i < overridenTraceIndexes.size(); ++i){
        sf.setTrace(overridenTraceIndexes[i], overridenTraces[i]);
    }
    sf.closeFile();

    SegyFile<double> sf2(segyFilePath, "r", SegyHeader::getDefaultMapping(),
                          SegyTraceHeader::getDefaultMapping(), Endian::ENDIAN_BIG);

    for(size_t i = 0; i < overridenTraceIndexes.size(); ++i){
        bool res=true;
        auto sftrace=sf2.getTrace(overridenTraceIndexes[i]);
        auto trace= overridenTraces[i];
        for(size_t j=0;j<trace.size();++j){
            res=res & isEqual(trace[j],sftrace->at(j));
        }
        QCOMPARE(res, true);
    }

}

QTEST_APPLESS_MAIN(SegyUnitTest)

#include "tst_segyunittest.moc"
