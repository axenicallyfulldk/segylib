#ifndef UTILS_H
#define UTILS_H

#include "types.h"

namespace segy_utils {
    int getFormatSize(Format format);

    bool isPositiveInt(const byte *buf, unsigned char length, Endian endian = currentEndian);

    int bytesToSignedInt(const byte *buf, unsigned char length, Endian endian = currentEndian);

    double bytesToDoubleFromIBM(const byte *buf, Endian endian);

    double bytesToDoubleFrom4IEEE(const byte *buf, Endian endian);

    double bytesToDoubleFrom8IEEE(const byte *buf, Endian endian);

    double bytesToDoubleFrom4FixedPoint(const byte* buf, Endian endian);

    double bytesToDouble(const byte *buf, Format format, Endian endian);

    void doubleToBytesSignedInt(const double num, byte* out, int length, Endian endian=currentEndian);
    void intToBytesSignedInt(const int num, byte* out, int length, Endian endian=currentEndian);

    void doubleToBytesIBM(const double num, byte* out, Endian endian);
    void doubleToBytes4IEEE(const double num, byte* out, Endian endian);
    void doubleToBytes8IEEE(const double num, byte* out, Endian endian);
    void doubleToBytes4FixedPoint(const double num, byte* out, Endian endian);
    void doubleToBytes(const double num, byte* out, Format format,Endian endian);

    void ebcdicToAscii (unsigned char *source, unsigned char *dest, size_t length);
    void asciiToEbcidic(unsigned char *source, unsigned char *dest, size_t length);

    long long calcSegyTraceCount(long long segySize, int samplePerTrace, int formatSize);
    long long calcSuTraceCount(long long suSize, int samplePerTrace, int formatSize);
}



#endif // UTILS_H
