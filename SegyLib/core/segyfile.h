#ifndef SEGYFILE
#define SEGYFILE

#include <cassert>

#include "types.h"
#include "utils.h"
#include "segyheader.h"
#include "segytraceheader.h"
#include "itracesobject.h"

#include <cstring>
#include <fstream>
#include <memory>
#include <map>
#include <set>
#include <vector>
#include <functional>
#include <algorithm>

#include "lrucache.h"

using namespace segy_utils;


/// Class for work with Segy files
template <typename TraceElementType = TraceElement,
          class TraceCacheType = LruCache<long long, std::vector<TraceElementType>>,
          class TraceHeaderCacheType = LruCache<long long, SegyTraceHeader>>
class SegyFile :public ITracesObject<std::vector<TraceElementType> >
{
public:
    using Trace = std::vector<TraceElementType>;
    using TracePtr = std::shared_ptr< std::vector<TraceElementType> >;
    using TraceConstPtr = std::shared_ptr<const std::vector<TraceElementType> >;

    SegyFile(const SegyHeader::Mapping& segyHeaderMapper = SegyHeader::getDefaultMapping(),
             const SegyTraceHeader::Mapping& segyTraceHeaderMapper = SegyTraceHeader::getDefaultMapping()):
        _isOpen(false), _traceCacheProxy(std::bind(&SegyFile::handleTraceReplacement, this)),
        _traceHeaderCacheProxy(std::bind(&SegyFile::handleTraceHeaderReplacement, this))
    {
        initHeaderMapper(segyHeaderMapper);
        initTraceHeaderMapping(segyTraceHeaderMapper);

        _traceCache.setMaxSize(100);
        _headerCache.setMaxSize(100);

        _traceCache.addRepObserver(&_traceCacheProxy);
        _headerCache.addRepObserver(&_traceHeaderCacheProxy);
    }

    SegyFile(String path, String mode = "rw",
             const SegyHeader::Mapping& segyHeaderMapper = SegyHeader::getDefaultMapping(),
             const SegyTraceHeader::Mapping& segyTraceHeaderMapper = SegyTraceHeader::getDefaultMapping(),
             Endian endian = Endian::ENDIAN_BIG):
        SegyFile(segyHeaderMapper,segyTraceHeaderMapper)
    {
        openFile(path, mode, endian);
    }

    virtual ~SegyFile(){
        fflush();
    }


    void openFile(String path, String mode = "rw", Endian endian = Endian::ENDIAN_BIG){
        assert(!isOpen() && "File is alredy opened");

        _endian = (endian == Endian::ENDIAN_UNDEFINED) ? Endian::ENDIAN_BIG : endian;

        if (mode == "w") _mode = Mode(false, true);
        else if (mode == "r")_mode = Mode(true, false);
        else if (mode == "rw" || mode == "wr") _mode = Mode(true, true);
        else _mode = Mode(true, true);

        auto streamMode = std::fstream::binary | std::fstream::in;
        if(_mode.write) streamMode |= std::fstream::out;

        _file.reset(new std::fstream(path, streamMode));

        if (!_file->good()){
            _isOpen = false;
            return;
        }

        _isOpen = true;
        _file->seekg(0, std::ios_base::beg);

        _file->read(_ebcidicHeader, 3200);

        fetchFileInfoFromHeader();
    }

    void closeFile(){
        fflush();

        _traceCache.clear();
        _headerCache.clear();

        _changedTraces.clear();
        _changedTraceHeaders.clear();

        _file.reset();
        _isOpen = false;
    }

    virtual bool isOpen()const{
        return _isOpen;
    }

    bool isWritable()const{
        return _mode.write;
    }

    Endian getEndian()const{
        return _endian;
    }

    void setEndian(Endian endian){
        if(endian == _endian || endian == Endian::ENDIAN_UNDEFINED) return;

        fflush();

        _traceCache.clear();
        _headerCache.clear();

        _endian = endian;

        fetchFileInfoFromHeader();
    }

    const SegyHeader::Mapping& getHeaderMapping() const{
        return _headerMapping;
    }

    void setHeaderMapping(const SegyHeader::Mapping& mapping){
        _headerMapping = mapping;

        if (isOpen()){
            SegyHeader header;
            readHeader(header);
            setHeader(header);
        }
    }

    const SegyTraceHeader::Mapping& getTraceHeaderMapping() const{
        return _traceHeaderMapping;
    }

    void setTraceHeaderMapping(const SegyTraceHeader::Mapping& mapping){
        fflushHeaders();
        _headerCache.clear();
        initTraceHeaderMapping(mapping);
    }

    //Write modified traces and trace headers to disk
    void fflush(){
        fflushHeaders();
        fflushTraces();
    }

    const char* getEbcidicHeader()const{
        return _ebcidicHeader;
    }

    void setEbcidicHeader(const char* header, size_t length = ebcidicHeaderSize){
        assert(_mode.write == true);

        std::fill(_ebcidicHeader, _ebcidicHeader + ebcidicHeaderSize, 0);

        length = std::max(length, (size_t)ebcidicHeaderSize);

        std::copy(header, header + length, _ebcidicHeader);

        char ebcidicData[ebcidicHeaderSize];
        std::copy(_ebcidicHeader, _ebcidicHeader + ebcidicHeaderSize, ebcidicData);

        _file->seekg(0, std::ios_base::beg);
        _file->write(ebcidicData, ebcidicHeaderSize);
    }

    int getSampleCount()const{
        return _sampleCount;
    }

    static SegyFile<TraceElementType>* createSegy(String path, const SegyHeader header, const Endian endian, const SegyHeader::Mapping * mapping = 0){
        std::fstream f(path, std::fstream::out | std::fstream::binary);

        if (!f.good()){
            return nullptr;
        }

        f.seekg(0, std::ios_base::beg);

        byte buff[lineHeaderSize];
        memset(buff, 0, lineHeaderSize);

        auto mmapper = mapping ? *mapping : SegyHeader::getDefaultMapping();

        segyHeaderToBinary(header, mmapper, endian,(char*) buff);

        f.seekg(ebcidicHeaderSize, std::ios_base::beg);
        f.write((char*)buff, lineHeaderSize);

        f.close();

        return new SegyFile<TraceElementType>(path, "rw", mmapper, SegyTraceHeader::getDefaultMapping(), endian);
    }

    const SegyHeader& getHeader()const{
        return _header;
    }

    void setHeader(const SegyHeader& header){
        assert(_mode.write);

        fflush();
        _traceCache.clear();
        _headerCache.clear();

        _header = header;
        writeHeaderToFile(_header);

        fetchFileInfoFromHeader();
    }

    virtual long long getTraceCount()const{
        return _totalTraceCount;
    }

    TraceConstPtr getTrace(long long traceNum) const{
        assert(_mode.read && isValidTraceNum(traceNum));

        if (_traceCache.hasElement(traceNum))
            return _traceCache.get(traceNum);

        std::shared_ptr<Trace> tracePtr(new Trace(getSampleCount(), 0));
        readTrace(traceNum, *tracePtr.get());
        _traceCache.put(traceNum, tracePtr);
        return tracePtr;
    }

    void setTrace(long long traceNum, const Trace &trace){
        assert(_mode.write);

        _traceCache.put(traceNum, std::shared_ptr<Trace>(new Trace(trace)));
        _changedTraces.insert(traceNum);
        if(_totalTraceCount <= traceNum) _totalTraceCount = traceNum + 1;
    }

    const SegyTraceHeader& getTraceHeader(long long traceNum)const{
        assert(_mode.read && isValidTraceNum(traceNum));

        if (_headerCache.hasElement(traceNum))
            return *_headerCache.get(traceNum);

        std::shared_ptr<SegyTraceHeader> sh(new SegyTraceHeader());
        readTraceHeader(traceNum, *sh.get());
        _headerCache.put(traceNum, sh);
        return *sh;
    }

    virtual void setTraceHeader(long long traceNum, const SegyTraceHeader& traceHeader){
        assert(_mode.write);

        std::shared_ptr<SegyTraceHeader> sth(new SegyTraceHeader(traceHeader));
        _headerCache.put(traceNum, sth);
        _changedTraceHeaders.insert(traceNum);

        if(_totalTraceCount <= traceNum) _totalTraceCount = traceNum + 1;
    }

public:
    static const int ebcidicHeaderSize = 3200;
    static const int lineHeaderSize = 400;
    static const int traceHeaderSize = 240;

protected:

    void handleTraceReplacement(){
        if (_changedTraces.size() > 0){
            for(auto it = _traceCache.repBegin(); it != _traceCache.repEnd(); ++it){
                fflushTrace(it.getKey());
            }
        }
    }

    void handleTraceHeaderReplacement(){
        if (_changedTraceHeaders.size() > 0){
            for(auto it = _headerCache.repBegin(); it != _headerCache.repEnd(); ++it){
                fflushTraceHeader(it.getKey());
            }
        }
    }

    bool isValidTraceNum(long long traceNum)const{
        return traceNum < getTraceCount() && traceNum >= 0;
    }

    bool isTraceChanged(long long traceNum)const{
        return _changedTraces.find(traceNum) != _changedTraces.end();
    }

    bool isTraceHeaderChanged(long long traceNum)const{
        return _changedTraceHeaders.find(traceNum) != _changedTraceHeaders.end();
    }

    void fflushTrace(long long traceNum)const{
        auto it = _changedTraces.find(traceNum);
        if (it != _changedTraces.end()){
            if(_traceCache.hasElement(traceNum)){
                auto trace=_traceCache.get(traceNum);
                writeTraceToFile(traceNum, *trace.get());
            }else if(_traceCache.hasReplacedElement(traceNum)){
                auto trace=_traceCache.getReplaced(traceNum);
                 writeTraceToFile(traceNum, *trace.get());
            }
            _changedTraces.erase(traceNum);
        }
    }

    void fflushTraceHeader(long long traceNum)const{
        if (_changedTraceHeaders.count(traceNum)){
            if(_headerCache.hasElement(traceNum)){
                writeTraceHeaderToFile(traceNum, *_headerCache.get(traceNum));
            }else if(_headerCache.hasReplacedElement(traceNum)){
                writeTraceHeaderToFile(traceNum, *_headerCache.getReplaced(traceNum));
            }
            _changedTraceHeaders.erase(traceNum);
        }
    }

    void fflushTraces()const{
        for(auto traceIndex : _changedTraces){
            TraceConstPtr tracePtr;
            if(_traceCache.hasElement(traceIndex)){
                tracePtr = _traceCache.get(traceIndex);
            }else if (_traceCache.hasReplacedElement(traceIndex)){
                tracePtr = _traceCache.getReplaced(traceIndex);
            }
            writeTraceToFile(traceIndex, *tracePtr);
        }
        _changedTraces.clear();
    }

    void fflushHeaders()const{
        for (auto traceIndex : _changedTraceHeaders){
            std::shared_ptr<const SegyTraceHeader> headerPtr;
            if(_headerCache.hasElement(traceIndex)){
                headerPtr = _headerCache.get(traceIndex);
            }else if (_headerCache.hasReplacedElement(traceIndex)){
                headerPtr = _headerCache.getReplaced(traceIndex);
            }
            writeTraceHeaderToFile(traceIndex, *headerPtr);
        }
        _changedTraceHeaders.clear();
    }

    // Fetch trace element format, sample count and calculating trace count
    void fetchFileInfoFromHeader(){
        readHeader(_header);

        //Get format from header
        _format = static_cast<Format>(_header.get(SegyHeader::type));
        //Get count of samples from header
        _sampleCount = _header.get(SegyHeader::sampleCount);

        //Calculating trace count
        _file->seekg(0, std::ios_base::end);
        //Calculatin sample count
        _totalTraceCount = calcSegyTraceCount(_file->tellg(), _sampleCount, getFormatSize(_format));
    }

    void initHeaderMapper(const SegyHeader::Mapping& segyHeaderMapper){
        _headerMapping = SegyHeader::getDefaultMapping();

        for (auto it : segyHeaderMapper){
            _headerMapping[it.first] = it.second;
        }
    }

    void initTraceHeaderMapping(const SegyTraceHeader::Mapping& segyTraceHeaderMapper){
        _traceHeaderMapping = SegyTraceHeader::getDefaultMapping();

        for (auto it : segyTraceHeaderMapper){
            _traceHeaderMapping[it.first] = it.second;
        }
    }

    void readHeader(SegyHeader& header)const{
        assert(isOpen());

        _file->seekg(ebcidicHeaderSize, std::ios_base::beg);

        std::vector<unsigned char> buff(lineHeaderSize,0);
        _file->read((char*)buff.data(), lineHeaderSize);

        for(auto field: header.fields){
            auto fieldMapping = _headerMapping.at(field);
            header.set(field, bytesToSignedInt(buff.data() + fieldMapping.offset, fieldMapping.length, _endian));
        }
    }

    void readTraceHeader(long long traceNum, SegyTraceHeader& traceHeader)const{
        assert(isOpen() && isValidTraceNum(traceNum));

        _file->seekg(getTraceOffset(traceNum), std::fstream::beg);

        std::vector<unsigned char> buff(traceHeaderSize,0);
        _file->read((char*)buff.data(), traceHeaderSize);

        for(auto field : traceHeader.fields){
            auto fieldMapping = _traceHeaderMapping.at(field);
            traceHeader.set(field, bytesToSignedInt(buff.data() + fieldMapping.offset, fieldMapping.length, _endian));
        }
    }

    void readTrace(long long traceNum, Trace& trace)const{
        assert(isOpen() && isValidTraceNum(traceNum));

        int formatSize = getFormatSize(_format);
        int length =  getSampleCount();
        std::vector<unsigned char> buff(formatSize*length,0);

        _file->seekg(getTraceOffset(traceNum) + traceHeaderSize, std::fstream::beg);
        _file->read((char*)buff.data(), formatSize*length);

        for (int i = 0; i<length; ++i)
            trace[i] = bytesToDouble(buff.data() + i*(formatSize), _format, _endian);
    }

    void writeTraceToFile(long long traceNum, const Trace& trace)const{
        assert(isOpen() && _mode.write);

        int formatSize = getFormatSize(_format);
        int sampleCount = getSampleCount();

        std::vector<char> traceBuf(sampleCount*formatSize, 0);
        byte* bufPtr = (byte*) traceBuf.data();

        for (int i = 0; i < sampleCount; ++i){
            doubleToBytes(trace[i], bufPtr, _format, _endian);
            bufPtr += formatSize;
        }

        _file->seekg(getTraceOffset(traceNum) + traceHeaderSize, std::fstream::beg);
        _file->write(traceBuf.data(), sampleCount*formatSize);
    }

    void writeTraceHeaderToFile(long long trace, const SegyTraceHeader& header)const{
        assert(isOpen() && _mode.write);

        std::vector<char> headerBuff(traceHeaderSize,0);
        segyTraceHeaderToBinary(header, _traceHeaderMapping, _endian, headerBuff.data());

        _file->seekg(getTraceOffset(trace), std::fstream::beg);
        _file->write(headerBuff.data(), traceHeaderSize);

        if(_totalTraceCount == trace+1){
            _file->seekg(0, std::ios_base::end);
            long long length = _file->tellg();
            auto nextTraceOffset=getTraceOffset(trace+1);
            if(length<nextTraceOffset){
                _file->seekg(nextTraceOffset-1);
                _file->write("",1);
            }
        }
    }

    void writeHeaderToFile(const SegyHeader& header)const{
        assert(isOpen() && _mode.write);

        std::vector<char> headerBuff(traceHeaderSize,0);
        segyHeaderToBinary(header, _headerMapping, _endian, headerBuff.data());
        _file->seekg(ebcidicHeaderSize, std::fstream::beg);
        _file->write(headerBuff.data(), lineHeaderSize);
    }

    static void segyTraceHeaderToBinary(const SegyTraceHeader& header,
                                        const SegyTraceHeader::Mapping& mapper,
                                        Endian endian, char* buff){
        std::fill(buff, buff + traceHeaderSize, 0);
        for(auto field : header.fields){
            auto fieldMapping = mapper.at(field);
            intToBytesSignedInt(header.get(field), (byte*)buff + fieldMapping.offset, fieldMapping.length, endian);
        }
    }

    static void segyHeaderToBinary(const SegyHeader& header,
                                   const SegyHeader::Mapping& mapping,
                                   Endian endian, char* buff){
        std::fill(buff, buff + lineHeaderSize, 0);
        for(auto field : header.fields){
            auto fieldMapping = mapping.at(field);
            intToBytesSignedInt(header.get(field), (byte*)buff + fieldMapping.offset, fieldMapping.length, endian);
        }
    }

    long long getTraceOffset(long long trace)const{
        long long traceLength = getSampleCount()*getFormatSize(_format);
        return ebcidicHeaderSize + lineHeaderSize + trace*(traceHeaderSize + traceLength);
    }

protected:
    struct Mode{
        bool read, write;
        Mode() :read(true), write(true){}
        Mode(bool r, bool w) :read(r), write(w){ }
    };

    mutable std::shared_ptr<std::fstream> _file;
    bool _isOpen;
    Mode _mode;
    long long _totalTraceCount;
    int _sampleCount;
    char _ebcidicHeader[3200];

    mutable TraceCacheType _traceCache;
    mutable TraceHeaderCacheType _headerCache;

    CacheObserverProxy<std::function<void()>> _traceCacheProxy;
    CacheObserverProxy<std::function<void()>> _traceHeaderCacheProxy;

    mutable std::set<long long> _changedTraces;
    mutable std::set<long long> _changedTraceHeaders;

    Endian _endian;
    Format _format;
    SegyHeader::Mapping _headerMapping;
    SegyTraceHeader::Mapping _traceHeaderMapping;

    SegyHeader _header;

};


#endif
