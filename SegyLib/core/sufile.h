#ifndef SUFILE_H
#define SUFILE_H


#include <cstring>
#include <fstream>
#include <map>
#include <vector>
#include <memory>
#include <set>
#include <functional>
#include <cassert>

#include "types.h"
#include "utils.h"
#include "itracesobject.h"
#include "segytraceheader.h"
#include "icache.h"
#include "lrucache.h"

using namespace segy_utils;

using SuTraceHeader = SegyTraceHeader;

template <class TraceElementType=TraceElement,
          class TraceCacheType=LruCache<long long, std::vector<TraceElementType> >,
          class TraceHeaderCacheType=LruCache<long long, SuTraceHeader>>
class SuFile :public ITracesObject<std::vector<TraceElementType> >
{

public:
    typedef std::vector<TraceElementType> Trace;
    typedef std::shared_ptr< std::vector<TraceElementType> > TracePtr;
    typedef std::shared_ptr<const std::vector<TraceElementType> > TraceConstPtr;

    SuFile(const SuTraceHeader::Mapping& suTraceHeaderMapping = SuTraceHeader::getDefaultMapping()):
        _isOpen(false), _traceCacheProxy(std::bind(&SuFile::handleTraceReplacement, this)),
        _traceHeaderCacheProxy(std::bind(&SuFile::handleTraceHeaderReplacement, this))
    {
        initTraceHeaderMapping(suTraceHeaderMapping);

        _traceCache.setMaxSize(100);
        _headerCache.setMaxSize(100);

        _traceCache.addRepObserver(&_traceCacheProxy);
        _headerCache.addRepObserver(&_traceHeaderCacheProxy);
    }

    SuFile(String path, size_t sampleCount, String mode = "rw",
           const SuTraceHeader::Mapping& suTraceHeaderMapping = SuTraceHeader::getDefaultMapping(),
           Format format = Format::FORMAT_IBM, Endian endian = Endian::ENDIAN_BIG):
        SuFile(suTraceHeaderMapping)
    {
        openFile(path, sampleCount, mode, format, endian);
    }

    virtual ~SuFile(){
        fflush();
    }

    void openFile(String path, size_t sampleCount, String mode = "rw", Format format=Format::FORMAT_IBM, Endian endian = Endian::ENDIAN_BIG){
        assert(!isOpen() && "File is alredy opened");

        _endian = (endian == Endian::ENDIAN_UNDEFINED) ? Endian::ENDIAN_BIG : endian;

        if (mode == "w") _mode = Mode(false, true);
        else if (mode == "r")_mode = Mode(true, false);
        else if (mode == "rw" || mode == "wr") _mode = Mode(true, true);
        else _mode = Mode(true, true);

        auto streamMode = std::fstream::binary | std::fstream::in;
        if(_mode.write) streamMode |= std::fstream::out;

        _file.reset(new std::fstream(path, streamMode));

        if (!_file->good()){
            _isOpen = false;
            return;
        }
        _isOpen = true;
        _file->seekg(0, std::ios_base::beg);

        _format = format;
        _sampleCount = sampleCount;

        _file->seekg(0, std::ios_base::end);
        _totalTraceCount = calcSuTraceCount(_file->tellg(), _sampleCount, getFormatSize(_format));
    }

    void closeFile(){
        fflush();

        _traceCache.clear();
        _headerCache.clear();

        _changedTraces.clear();
        _changedTraceHeaders.clear();

        _file.reset();
        _isOpen = false;
    }

    bool isOpen()const{
        return _isOpen;
    }

    bool isWritable()const{
        return _mode.write;
    }

    Endian getEndian()const{
        return _endian;
    }

    void setEndian(Endian endian){
        if(endian == _endian || endian == Endian::ENDIAN_UNDEFINED) return;

        fflush();

        _traceCache.clear();
        _headerCache.clear();

        _endian = endian;
    }

    const SuTraceHeader::Mapping& getTraceHeaderMapping() const{
        return _traceHeaderMapping;
    }

    void setTraceHeaderMapping(const SegyTraceHeader::Mapping& mapping){
        fflushHeaders();
        _headerCache.clear();
        initTraceHeaderMapping(mapping);
    }

    void fflush(){
        fflushHeaders();
        fflushTraces();
    }

    int getSampleCount()const{
        return _sampleCount;
    }

    static SuFile<TraceElementType>* createSu(String path, size_t sampleCount,
                                              Format format, const Endian endian,
                                              const SuTraceHeader::Mapping * mapping = 0){
        std::fstream f(path, std::fstream::out | std::fstream::binary);
        if(!f.good()) return nullptr;
        f.seekg(0, std::ios_base::beg);
        f.close();

        auto mmapper = mapping ? *mapping : SuTraceHeader::getDefaultMapping();

        return new SuFile<TraceElementType>(path, sampleCount, "rw", mmapper, format, endian);
    }

    virtual long long getTraceCount()const{
        return _totalTraceCount;
    }

    virtual TraceConstPtr getTrace(long long traceNum)const{
        assert(_mode.read && isValidTraceNum(traceNum));

        if (_traceCache.hasElement(traceNum)) return _traceCache.get(traceNum);

        auto tracePtr = std::shared_ptr<Trace>(new Trace(_sampleCount, 0));
        readTrace(traceNum, *(tracePtr.get()));
        _traceCache.put(traceNum, tracePtr);
        return tracePtr;
    }

    virtual void setTrace(long long traceNum, const Trace& trace){
        assert(_mode.write);
        _traceCache.put(traceNum, std::shared_ptr<Trace>(new Trace(trace)));
        _changedTraces.insert(traceNum);
        if(_totalTraceCount <= traceNum) _totalTraceCount = traceNum + 1;
    }

    const SuTraceHeader& getTraceHeader(long long traceNum)const{
        assert(_mode.read && isValidTraceNum(traceNum));

        if (_headerCache.hasElement(traceNum)) return *_headerCache.get(traceNum);

        std::shared_ptr<SuTraceHeader> sh(new SuTraceHeader());
        readTraceHeader(traceNum, *sh.get());
        _headerCache.add(traceNum, sh);
        return *_headerCache.get(traceNum);
    }

    virtual void setTraceHeader(long long traceNum, const SuTraceHeader& traceHeader){
        assert(_mode.write);

        auto sth=std::shared_ptr<SuTraceHeader>(new SuTraceHeader(traceHeader));
        _headerCache.put(traceNum, sth);
        _changedTraceHeaders.insert(traceNum);

        if(_totalTraceCount <= traceNum) _totalTraceCount = traceNum + 1;
    }

public:
    static const int traceHeaderSize = 240;

protected:
    void handleTraceReplacement(){
        if (_changedTraces.size() > 0){
            for(auto it = _traceCache.repBegin(); it != _traceCache.repEnd(); ++it){
                fflushTrace(it.getKey());
            }
        }
    }

    void handleTraceHeaderReplacement(){
        if (_changedTraceHeaders.size() > 0){
            for(auto it = _headerCache.repBegin(); it != _headerCache.repEnd(); ++it){
                fflushTraceHeader(it.getKey());
            }
        }
    }

    bool isValidTraceNum(long long traceNum) const{
        return traceNum < getTraceCount();
    }

    bool isTraceChanged(long long traceNum) const{
        return (_changedTraces.find(traceNum) != _changedTraces.end());
    }

    bool isTraceHeaderChanged(long long traceNum) const{
        return (_changedTraceHeaders.find(traceNum) != _changedTraceHeaders.end());
    }

    void fflushTrace(long long traceNum)const{
        if (_changedTraces.count(traceNum)){
            if(_traceCache.hasElement(traceNum)){
                writeTraceToFile(traceNum, *_traceCache.get(traceNum));
            }else if(_traceCache.hasReplacedElement(traceNum)){
                 writeTraceToFile(traceNum, *_traceCache.getReplaced(traceNum));
            }
            _changedTraces.erase(traceNum);
        }
    }

    void fflushTraceHeader(long long traceNum)const{
        if (_changedTraceHeaders.count(traceNum)){
            if(_headerCache.hasElement(traceNum)){
                writeTraceHeaderToFile(traceNum, *_headerCache.get(traceNum));
            }else if(_headerCache.hasReplacedElement(traceNum)){
                writeTraceHeaderToFile(traceNum, *_headerCache.getReplaced(traceNum));
            }
            _changedTraceHeaders.erase(traceNum);
        }
    }


    void fflushTraces()const{
        for(auto traceIndex : _changedTraces){
            writeTraceToFile(traceIndex, *_traceCache.get(traceIndex));
        }
        _traceCache.clear();
    }

    void fflushHeaders()const{
        for(auto traceIndex : _changedTraceHeaders){
            writeTraceHeaderToFile(traceIndex, *_headerCache.get(traceIndex));
        }
        _headerCache.clear();
    }

    void initTraceHeaderMapping(const SuTraceHeader::Mapping& mapping){
        _traceHeaderMapping = SuTraceHeader::getDefaultMapping();
        for(auto field: mapping){
            _traceHeaderMapping[field.first] = field.second;
        }
    }

    void readTraceHeader(long long traceNum, SuTraceHeader& traceHeader)const{
        assert(isOpen() && isValidTraceNum(traceNum));

        _file->seekg(getTraceOffset(traceNum), std::fstream::beg);
        std::vector<unsigned char> buff(traceHeaderSize, 0);
        _file->read((char*)buff.data(), traceHeaderSize);

        for(auto field : SuTraceHeader::fields){
            auto fieldMapping = _traceHeaderMapping.at(field);
            traceHeader.set(field, bytesToSignedInt(buff.data() + fieldMapping.offset, fieldMapping.length, _endian));
        }
    }

    void readTrace(long long traceNum, Trace& trace)const{
        assert(isOpen() && isValidTraceNum(traceNum));
        _file->seekg(getTraceOffset(traceNum) + traceHeaderSize, std::fstream::beg);
        int formatSize = getFormatSize(_format);
        int length =  getSampleCount();
        std::vector<unsigned char> buff(formatSize*length, 0);
        _file->read((char*)buff.data(), formatSize*length);

        for (int i = 0; i<length; ++i)
            trace[i] = bytesToDouble(buff.data() + i*(formatSize), _format, _endian);
    }

    void writeTraceToFile(long long traceNum, const Trace& trace)const{
        assert(isOpen() && _mode.write);
        int formatSize = getFormatSize(_format), sampleCount = getSampleCount();
        std::vector<char> traceBuf(sampleCount*formatSize, 0);
        for (int i = 0; i < sampleCount; ++i){
            doubleToBytes(trace[i], (byte*)traceBuf.data() + i*formatSize, _format, _endian);
        }
        _file->seekg(getTraceOffset(traceNum)+traceHeaderSize, std::fstream::beg);
        _file->write(traceBuf.data(), sampleCount*formatSize);
    }

    void writeTraceHeaderToFile(long long trace, const SuTraceHeader& header)const{
        if (!isOpen()&&!_mode.write)throw;
        std::vector<char> headerBuff(traceHeaderSize, 0);
        suTraceHeaderToBinary(header, _traceHeaderMapping, _endian, headerBuff.data());
        _file->seekg(getTraceOffset(trace), std::fstream::beg);
        _file->write(headerBuff.data(), traceHeaderSize);
    }

    static void suTraceHeaderToBinary(const SuTraceHeader& header, const SuTraceHeader::Mapping& mapping, Endian endian, char* buff){
        std::fill(buff, buff + traceHeaderSize, 0);
        for(auto field : SuTraceHeader::fields){
            auto fieldMapping = mapping.at(field);
            intToBytesSignedInt(header.get(field), (byte*)buff + fieldMapping.offset, fieldMapping.length, endian);
        }
    }

    long long getTraceOffset(long long trace)const{
        long long traceLength = getSampleCount()*getFormatSize(_format);
        return trace*(traceHeaderSize + traceLength);
    }

protected:

    struct Mode{
        bool read, write;
        Mode() :read(true), write(true){}
        Mode(bool r, bool w) :read(r), write(w){ }
    };

    mutable std::shared_ptr < std::fstream> _file;
    bool _isOpen;
    Mode _mode;
    long long _totalTraceCount;
    int _sampleCount;

    mutable TraceCacheType _traceCache;
    mutable TraceHeaderCacheType _headerCache;

    CacheObserverProxy<std::function<void()>> _traceCacheProxy;
    CacheObserverProxy<std::function<void()>> _traceHeaderCacheProxy;

    mutable std::set<long long> _changedTraces;
    mutable std::set<long long> _changedTraceHeaders;

    Endian _endian;
    Format _format;
    SuTraceHeader::Mapping _traceHeaderMapping;
};


#endif // SUFILE_H
