#include "segyheader.h"

using namespace std;

const std::array<SegyHeader::Field, SegyHeader::fixedLength-SegyHeader::jobId + 1> SegyHeader::fields = {
    SegyHeader::jobId, SegyHeader::lineNumber, SegyHeader::reelNumber,
    SegyHeader::numTracesPerRecord, SegyHeader::numOfAxualiryTracesPerRecord,
    SegyHeader::sampleInterval, SegyHeader::sampleIntervalOrigin,
    SegyHeader::sampleCount, SegyHeader::sampleCountOrigin, SegyHeader::type,
    SegyHeader::cdpFold, SegyHeader::traceSorting, SegyHeader::verticalSum,
    SegyHeader::sweepFrStart, SegyHeader::sweepFrEnd, SegyHeader::sweepLength,
    SegyHeader::sweepType, SegyHeader::traceNumOfSweepChannel,
    SegyHeader::sweepTaperLengthStart, SegyHeader::sweepTaperLengthEnd,
    SegyHeader::taperType, SegyHeader::corrDataTraces,
    SegyHeader::binGainRecovery, SegyHeader::ampRecoveryMethod,
    SegyHeader::measurementSys, SegyHeader::impSignal, SegyHeader::polarityCode,
    SegyHeader::segyFormat, SegyHeader::fixedLength
};

SegyHeader::SegyHeader(){
    for(auto field: fields){
        fieldValue[field] = 0;
    }
}

int SegyHeader::get(Field field) const{
    auto it = fieldValue.find(field);
    if(it != fieldValue.end()){
        return it->second;
    }

    return -1;
}

void SegyHeader::set(Field field, int value){
    fieldValue[field] = value;
}

const FieldInfo &SegyHeader::getFieldInfo(SegyHeader::Field field){
    return _detailInfo.at(field);
}

const std::map<SegyHeader::Field, FieldMapping> &SegyHeader::getDefaultMapping(){
    return _defaultMapping;
}

static std::map < SegyHeader::Field, FieldMapping> initMapperInfo(){
    std::map<SegyHeader::Field, FieldMapping> mmap;
    mmap[SegyHeader::jobId] = FieldMapping(0, 4);
    mmap[SegyHeader::lineNumber] = FieldMapping(4, 4);
    mmap[SegyHeader::reelNumber] = FieldMapping(8, 4);
    mmap[SegyHeader::numTracesPerRecord] = FieldMapping(12, 2);
    mmap[SegyHeader::numOfAxualiryTracesPerRecord] = FieldMapping(14, 2);
    mmap[SegyHeader::sampleInterval] = FieldMapping(16, 2);
    mmap[SegyHeader::sampleIntervalOrigin] = FieldMapping(18, 2);
    mmap[SegyHeader::sampleCount] = FieldMapping(20, 2);
    mmap[SegyHeader::sampleCountOrigin] = FieldMapping(22, 2);
    mmap[SegyHeader::type] = FieldMapping(24, 2);
    mmap[SegyHeader::cdpFold] = FieldMapping(26, 2);
    mmap[SegyHeader::traceSorting] = FieldMapping(28, 2);
    mmap[SegyHeader::verticalSum] = FieldMapping(30, 2);
    mmap[SegyHeader::sweepFrStart] = FieldMapping(32, 2);
    mmap[SegyHeader::sweepFrEnd] = FieldMapping(34, 2);
    mmap[SegyHeader::sweepLength] = FieldMapping(36, 2);
    mmap[SegyHeader::sweepType] = FieldMapping(38, 2);
    mmap[SegyHeader::traceNumOfSweepChannel] = FieldMapping(40, 2);
    mmap[SegyHeader::sweepTaperLengthStart] = FieldMapping(42, 2);
    mmap[SegyHeader::sweepTaperLengthEnd] = FieldMapping(44, 2);
    mmap[SegyHeader::taperType] = FieldMapping(46, 2);
    mmap[SegyHeader::corrDataTraces] = FieldMapping(48, 2);
    mmap[SegyHeader::binGainRecovery] = FieldMapping(50, 2);
    mmap[SegyHeader::ampRecoveryMethod] = FieldMapping(52, 2);
    mmap[SegyHeader::measurementSys] = FieldMapping(54, 2);
    mmap[SegyHeader::impSignal] = FieldMapping(56, 2);
    mmap[SegyHeader::polarityCode] = FieldMapping(58, 2);
    mmap[SegyHeader::segyFormat]=FieldMapping(300,2);
    mmap[SegyHeader::fixedLength]=FieldMapping(302,2);
    return mmap;
}

const std::map < SegyHeader::Field, FieldMapping> SegyHeader::_defaultMapping = initMapperInfo();



static map < SegyHeader::Field, FieldInfo> initDetailInfo(){
    map<SegyHeader::Field, FieldInfo> mmap;
    mmap.insert(make_pair(SegyHeader::jobId, FieldInfo("jobId", "Job identification number", "Job identification number")));
    mmap.insert(make_pair(SegyHeader::lineNumber, FieldInfo("lineNumber", "Line number","For 3-D poststack data, this will typically contain the in-line number")));
    mmap.insert(make_pair(SegyHeader::reelNumber, FieldInfo("reelNumber", "Reel number","Reel number")));
    mmap.insert(make_pair(SegyHeader::numTracesPerRecord, FieldInfo("numTracesPerRecord", "Number of data traces per ensemble","Number of data traces per ensemble (Mandatory for prestack data)")));
    mmap.insert(make_pair(SegyHeader::numOfAxualiryTracesPerRecord, FieldInfo("numOfAxualiryTracesPerRecord", "Number of auxiliary traces per ensemble","Number of auxiliary traces per ensemble (Mandatory for prestack data)")));
    mmap.insert(make_pair(SegyHeader::sampleInterval, FieldInfo("sampleInterval", "Sample interval in microseconds","Sample interval in microseconds (Mandatory for prestack data)")));
    mmap.insert(make_pair(SegyHeader::sampleIntervalOrigin, FieldInfo("sampleIntervalOrigin", "Sample interval in microseconds of original field recording","Sample interval in microseconds of original field recording")));
    mmap.insert(make_pair(SegyHeader::sampleCount, FieldInfo("sampleCount", "Number of samples per data trace","Number of samples per data trace (Mandatory for all types of data)\nThe sample interval and number of samples in the Binary File Header should be for the primary set of seismic data traces in the file.")));
    mmap.insert(make_pair(SegyHeader::sampleCountOrigin, FieldInfo("sampleCountOrigin", "Number of samples per data trace for original field recording","Number of samples per data trace for original field recording")));
    mmap.insert(make_pair(SegyHeader::type, FieldInfo("type", "Data sample format code","Data sample format code (Mandatory for all data)\n1=4-byte IBM floating-point\n2=4-byte, two's complement integer\n3=2-byte, two's complement integer\n4=4-byte fixed-pointwith gain (obsolete)\n5=4-byte IEEE floating-point\n6=Not currently used\n7=Not currently used\n8=1-byte, two's complement integer")));
    mmap.insert(make_pair(SegyHeader::cdpFold, FieldInfo("cdpFold", "Ensemble fold","The expected number of data taces per trace ensemble (e.g. the CMP fold). Highly recommended for all types of data.")));
    mmap.insert(make_pair(SegyHeader::traceSorting, FieldInfo("traceSorting", "Trace sorting code","Trace sorting code (i.e. type of ensemble):\n-1=Other (should be explained in user Extended Textual File Header stanza\n0=Unknown\n1=As recorded (no sorting)\n2=CDP ensemble\n3=Single fold continuous profile\n4=Horizontally stacked\n5=Common source point\n6=Common receiver point\n7=Common offset point\n8=Common mid-point\n9=Common conversion point")));
    mmap.insert(make_pair(SegyHeader::verticalSum, FieldInfo("verticalSum", "Vertical sum code")));
    mmap.insert(make_pair(SegyHeader::sweepFrStart, FieldInfo("sweepFrStart", "Sweep frequency at start","Sweep frequency at start (Hz)")));
    mmap.insert(make_pair(SegyHeader::sweepFrEnd, FieldInfo("sweepFrEnd", "Sweep frequency at end","Sweep frequency at end (Hz)")));
    mmap.insert(make_pair(SegyHeader::sweepLength, FieldInfo("sweepLength", "Sweep length","Sweep length (miliseconds)")));
    mmap.insert(make_pair(SegyHeader::sweepType, FieldInfo("sweepType", "Sweep type code","Sweep type code:\n1=linear\n2=parabolic\n3=exponential\n4=other")));
    mmap.insert(make_pair(SegyHeader::traceNumOfSweepChannel, FieldInfo("traceNumOfSweepChannel", "Trace number of sweep channel")));
    mmap.insert(make_pair(SegyHeader::sweepTaperLengthStart, FieldInfo("sweepTaperLengthStart", "Sweep trace taper length at start","Sweep trace taper length in milliseconds at start if tapered (the taper starts at zero time and is effective fo tis length)")));
    mmap.insert(make_pair(SegyHeader::sweepTaperLengthEnd, FieldInfo("sweepTaperLengthEnd", "Sweep trace taper length at end","Sweep trace taper length in milliseconds at end (the ending taper start at seep length minus the taper length at end)")));
    mmap.insert(make_pair(SegyHeader::taperType, FieldInfo("taperType", "Taper type","Taper type:\n1=linear\n2=cos^2\n3=other")));
    mmap.insert(make_pair(SegyHeader::corrDataTraces, FieldInfo("corrDataTraces", "Correlated data traces","Correlated data traces:\n1=no\n2=yes")));
    mmap.insert(make_pair(SegyHeader::binGainRecovery, FieldInfo("binGainRecovery", "Binary gain recovered","Binary gain recovered:\n1=yes\n2=no")));
    mmap.insert(make_pair(SegyHeader::ampRecoveryMethod, FieldInfo("ampRecoveryMethod", "Amplitude recovery metod","Amplitued recovery method:\n1=none\n2=spherical divergence\n3=AGC\n4=other")));
    mmap.insert(make_pair(SegyHeader::measurementSys, FieldInfo("measurementSys", "Measurement system","Measurement system (Highly recommended for all types of data). If Location Data stanzas are included in the file, this entry must agree with the Location Data stanza. If there is a disagreement, the last Location Data stanza i the controlling autority.\n1=Meters\n2=Feet")));
    mmap.insert(make_pair(SegyHeader::impSignal, FieldInfo("impSignal", "Impulse signal polarity","1=Increase in pressure or upward geophone case movement gives negative number on tape\n2=Increase in pressure or upward geophone case movement gives positive number on tape.")));
    mmap.insert(make_pair(SegyHeader::polarityCode, FieldInfo("polarityCode", "Vibratory polarity code","Seismic signal lags pilot signal by:\n1=337.5 to 22.5\n2=22.5 to 67.5\n3=67.5 to 112.5\n4=112.5 to 157.5\n5=157.5 to 202.5\n6=202.5 to 247.5\n7=247.5 to 292.5\n8=292.5 to 337.5")));
    mmap.insert(make_pair(SegyHeader::segyFormat, FieldInfo("segyFormat", "SEGY Format Revision Number","This is a 16-bit unsigned value with a Q-point between the first and second bytes. Thus for SEGY Revision 1.0, as difined in this document, this will be recorded as 0100_16. This field is mandatory for all version of SEGY, although a value of zero indicates \"traditional\" SEGY conforming to the 1975 standard.")));
    mmap.insert(make_pair(SegyHeader::fixedLength, FieldInfo("fixedLength", "Fixed length trace flag","A value of one indicates that all trace3s in this SEGY file are guaranteed to have the same sample interval and number of samples, as specified in Textual File Header bytes 3217-3218 and 3221-3222. A value of zero indicates that the length of the tracs in the file may vary and the number of sample in bytes 115-116 of the Trace Header must be examined to determine the actual lnegth of each trace. This field is mandatory for all version of SEGY, although a value of zero indicates \"traditional\" SEGY conforming to the 1975 standard.")));
    return mmap;
}

const map <SegyHeader::Field, FieldInfo> SegyHeader::_detailInfo = initDetailInfo();
