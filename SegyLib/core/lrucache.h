#ifndef LRUCACHE_H
#define LRUCACHE_H

#include "icache.h"

#include <list>

/// Class that implements LRU cache
template <class Key, class Value>
class LruCache :public ICache<Key, Value>{
public:
    typedef std::shared_ptr<Value> ValuePtr;
    typedef std::shared_ptr<const Value> ConstValuePtr;

private:

    struct ValuePriority;

    // Type for iterating through element container
    template<bool isConstant>
    using DataMapIterator = std::conditional_t<isConstant, typename std::map<Key, ValuePriority>::const_iterator, typename std::map<Key, ValuePriority>::iterator>;

    // Type for iterating through replaced element container
    template<bool isConstant>
    using RepMapIterator = std::conditional_t<isConstant, typename std::map<Key, ValuePtr>::const_iterator, typename std::map<Key, ValuePtr>::iterator>;

    // Type for iterating by elements in priority list
    using PriorityIterator = typename std::list<DataMapIterator<false>>::iterator;

    // Structure that contains value and iterator of value in the priority list
    struct ValuePriority{
        ValuePtr val;
        PriorityIterator priority;
        ValuePriority(ValuePtr v, PriorityIterator mi = PriorityIterator()):val(v), priority(mi){}
    };

    // Functor class for extracing value from iterator
    template<bool isConstant>
    struct ValueExtractor{
        ValuePtr operator()(const DataMapIterator<isConstant>& vi) const{
            return vi->second.val;
        }
    };

    // Functor class for extracting value from iterator by replaced elments
    template<bool isConstant>
    struct ReplacedValueExtractor{
        ValuePtr operator()(const RepMapIterator<isConstant>& vi) const{
            return vi->second;
        }
    };

    // Priority list
    std::list<DataMapIterator<false>> _priorities;
    std::map<Key, ValuePriority> _data;
    std::map<Key, ValuePtr> _replacedData;

    std::list<ICacheReplacementObserver*> _observers;
    size_t cacheSize;

    template<bool isConstant, typename MapIteratorType, typename ValueExtractor>
    class LruIterator: public ICache<Key, Value>::template IIteratorImpl<isConstant>{
    private:
        using BaseType = typename ICache<Key, Value>::template IIteratorImpl<isConstant>;
        using ValuePtr = typename BaseType::ValuePtr;

        MapIteratorType _it;
        ValueExtractor _vf;

        LruIterator(const LruIterator<isConstant, MapIteratorType, ValueExtractor>& it){
            _it = it._it;
        }

        LruIterator(const MapIteratorType& it){
            _it = it;
        }

        friend class LruCache;
        friend class LruIterator<!isConstant, MapIteratorType, ValueExtractor>;

    public:
        virtual const Key& getKey()const{
            return _it->first;
        }

        virtual ValuePtr getValue()const{
            return _vf(_it);
        }

        virtual bool operator==(const BaseType& it)const{
             auto& iit = dynamic_cast<const LruIterator<isConstant, MapIteratorType, ValueExtractor>&>(it);
             return _it == iit._it;
        }

        virtual LruIterator<isConstant, MapIteratorType, ValueExtractor>* clone() const{
            return new LruIterator<isConstant, MapIteratorType, ValueExtractor>(*this);
        }

        const LruIterator<isConstant, MapIteratorType, ValueExtractor> &operator ++(){
            ++_it;
            return *this;
        }
    };

    // Class for iterating through cache
    template<bool isConstant>
    using LruValueIterator = LruIterator<isConstant, DataMapIterator<isConstant>, ValueExtractor<isConstant>>;

    // Class for iterating through replaced elements of the cache
    template<bool isConstant>
    using LruRepValueIterator = LruIterator<isConstant, RepMapIterator<isConstant>, ReplacedValueExtractor<isConstant>>;

public:

    using iterator = typename ICache<Key, Value>::iterator;
    using const_iterator = typename ICache<Key, Value>::const_iterator;

    LruCache(size_t cs = 100):cacheSize(cs){}

    virtual size_t getMaxSize() const{
        return cacheSize;
    }

    virtual void setMaxSize(size_t s){
        cacheSize = s;
    }

    virtual void put(const Key& key, ValuePtr value){

        auto it = _data.find(key);
        if(it == _data.end()){
            it = _data.emplace(key, ValuePriority(value)).first;
        }else{
            _priorities.erase(it->second.priority);
            it->second.val = value;
        }
        _priorities.push_back(it);
        it->second.priority =(--_priorities.end());

        while (_data.size() > cacheSize){
            auto valIt = _priorities.front();
            _replacedData[valIt->first] = valIt->second.val;
            _priorities.pop_front();
            _data.erase(valIt);
        }

        if(_replacedData.size()){
            notifyRepObservers();
            _replacedData.clear();
        }
    }

    virtual std::shared_ptr<Value> remove(const Key& key){
        std::shared_ptr<Value> val;

        auto it = _data.find(key);
        if (it != _data.end()){
            val = it->second.val;
            _priorities.erase(it->second.priority);
            _data.erase(it);
        }
        return val;
    }

    virtual void clear(){
        _data.clear();
        _priorities.clear();
        _replacedData.clear();
    }

    virtual size_t size() const{
        return _data.size();
    }

    virtual bool hasElement(const Key& key)const{
        return _data.find(key) != _data.end();
    }

    virtual ValuePtr get(const Key& key){
        ValuePtr val;

        auto it = _data.find(key);
        if (it != _data.end())
            val = it->second.val;

        return val;
    }

    virtual ConstValuePtr get(const Key& key) const {
        auto it=_data.find(key);
        ConstValuePtr val;
        if (it!=_data.end())
            val = it->second.val;
        return val;
    }

    virtual iterator begin(){
        auto it = new LruValueIterator<false>(_data.begin());
        return ICache<Key,Value>::createIterator(it);
    }
    virtual iterator end(){
        auto it = new LruValueIterator<false>(_data.end());
        return ICache<Key,Value>::createIterator(it);
    }

    virtual const_iterator begin() const{
        auto it = new LruValueIterator<true>(_data.begin());
        return ICache<Key,Value>::createIterator(it);
    }
    virtual const_iterator end() const{
        auto it = new LruValueIterator<true>(_data.end());
        return ICache<Key,Value>::createIterator(it);
    }

    virtual size_t repSize() const{
        return _replacedData.size();
    }

    virtual bool hasReplacedElement(const Key& key) const{
        return _replacedData.find(key) != _replacedData.end();
    }

    virtual ConstValuePtr getReplaced(const Key& key) const {
        ValuePtr val;
        auto it = _replacedData.find(key);
        if (it != _replacedData.end())
            val = it->second;
        return val;
    }

    virtual iterator repBegin(){
        auto it = new LruRepValueIterator<false>(_replacedData.begin());
        return ICache<Key,Value>::createIterator(it);
    }
    virtual iterator repEnd(){
        auto it = new LruRepValueIterator<false>(_replacedData.end());
        return ICache<Key,Value>::createIterator(it);
    }

    virtual const_iterator repBegin() const{
        auto it = new LruRepValueIterator<true>(_replacedData.begin());
        return ICache<Key,Value>::createIterator(it);
    }
    virtual const_iterator repEnd() const{
        auto it = new LruRepValueIterator<true>(_replacedData.end());
        return ICache<Key,Value>::createIterator(it);
    }

    virtual void addRepObserver(ICacheReplacementObserver* observer){
        auto it = std::find(_observers.begin(), _observers.end(), observer);
        if (it == _observers.end())
            _observers.push_back(observer);
    }

    virtual void removeRepObserver(ICacheReplacementObserver* observer) {
        auto it = std::find(_observers.begin(), _observers.end(), observer);
        _observers.erase(it);
    }

    virtual void notifyRepObservers()const{
        for (auto it= _observers.begin(); it != _observers.end(); ++it){
            (*it)->handleReplacement();
        }
    }

};


#endif // LRUCACHE_H
