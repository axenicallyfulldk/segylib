#ifndef ITRACEFILE_H
#define ITRACEFILE_H

#include <memory>

/// Interface for object that contains traces
template<class TraceType>
class ITracesObject
{
public:
    typedef std::shared_ptr<const TraceType> TracePtr;

    virtual long long getTraceCount() const = 0;

    virtual TracePtr getTrace(long long traceNum) const = 0;

    virtual void setTrace(long long traceNum, const TraceType& trace) = 0;
};

#endif // ITRACEFILE_H
