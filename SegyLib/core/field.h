#ifndef FIELD_H
#define FIELD_H

#include <string>

struct FieldInfo{
    std::string fieldName;
    std::string fullFieldName;
    std::string fieldDescription;

    FieldInfo(const std::string& fn, const std::string& ffn = std::string(),
              const std::string& fd = std::string()):
        fieldName(fn), fullFieldName(ffn), fieldDescription(fd){}
};

struct FieldMapping{
    int offset;
    int length;

    FieldMapping(int sp, int l) :offset(sp), length(l){}
    FieldMapping() :offset(0), length(0){}
};


#endif // FIELD_H
