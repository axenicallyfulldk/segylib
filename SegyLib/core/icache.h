#ifndef ICACHE_H
#define ICACHE_H

#include <algorithm>
#include <memory>
#include <map>
#include <type_traits>


class ICacheReplacementObserver{
public:
    virtual void handleReplacement() = 0;
};

// Class to redirect cache observation events
template<class TargetType>
class CacheObserverProxy: public ICacheReplacementObserver{
    TargetType _target;

public:
    CacheObserverProxy(TargetType target):_target(target){}

    void handleReplacement(){
        _target();
    }
};

/// Interface for cache classes
template<class Key, class Value>
class ICache
{
protected:
    template <bool isConstant>
    class IIteratorImpl{
    public:
        using ValuePtr = std::conditional_t<isConstant, std::shared_ptr<const Value>, std::shared_ptr<Value>>;

        virtual const Key& getKey()const = 0;
        virtual ValuePtr getValue() const= 0;

        virtual bool operator==(const IIteratorImpl& it)const = 0;

        virtual const IIteratorImpl<isConstant>& operator++() = 0;

        virtual IIteratorImpl<isConstant>* clone() const = 0;
    };

    /// Iterator class, that wraps iterator's implementation
    template <bool isConstant>
    class Iterator{
    private:
        friend class ICache;

        std::unique_ptr<IIteratorImpl<isConstant>> _impl;

        Iterator(std::unique_ptr<IIteratorImpl<isConstant>>&& it):_impl(it){
        }


        Iterator(IIteratorImpl<isConstant>* it):_impl(it){
        }

    public:

        using iterator_category = std::forward_iterator_tag;
        using difference_type = ptrdiff_t;
        using value_type = Value;
        using pointer = std::conditional_t<isConstant, std::shared_ptr<const Value>, std::shared_ptr<Value>>;
        using reference = std::conditional_t<isConstant, std::shared_ptr<const Value>, std::shared_ptr<Value>>;

        const Key& getKey()const {
            return _impl->getKey();
        }

        pointer getValue()const{
            return _impl->getValue();
        }

        Iterator(const Iterator<isConstant>& it):_impl(it._impl->clone()){
        }

        Iterator<isConstant>& operator=(Iterator<isConstant>& it){
            _impl.reset(it._impl->clone());
            return *this;
        }

        /// pre-increment
        Iterator<isConstant>& operator++(){
            ++(*_impl);
            return *this;
        }

        /// post-increment
        Iterator<isConstant> operator++(int){
            Iterator<isConstant> old = *this;
            ++(*this);
            return old;
        }

        pointer operator*()const{
            return getValue();
        }

        operator Iterator<true>()const{
            return Iterator<true>(_impl->clone());
        }

        template<bool R>
        bool operator==(const Iterator<R>& it) const{
            return (*_impl) == (*it._impl);
        }

        template<bool R>
        bool operator!=(const Iterator<R>& it) const{
            return !(*this == it);
        }
    };

    template<bool isConstant>
    Iterator<isConstant> createIterator(IIteratorImpl<isConstant>* iit)const{
        return Iterator<isConstant>(iit);
    }

public:

    using iterator = Iterator<false>;
    using const_iterator = Iterator<true>;

    using ValuePtr = std::shared_ptr<Value>;
    using ConstValuePtr = std::shared_ptr<const Value>;

    /// Gets and sets cache max size
    virtual size_t getMaxSize() const = 0;
    virtual void setMaxSize(size_t s) = 0;

    /// Puts value in the cache. If element with given value is already in the
    /// cache then updates its value
    virtual void put(const Key& key, ValuePtr value) = 0;

    /// Removes element
    virtual ValuePtr remove(const Key& key) = 0;

    /// Removes all elements from the cache
    virtual void clear() = 0;

    /// Returns count of elements in the cache
    virtual size_t size()const = 0;

    /// Returns true if the element is in the cache
    virtual bool hasElement(const Key& key) const = 0;

    /// Returns pointer to value
    virtual ValuePtr get(const Key& key)= 0;
    virtual ConstValuePtr get(const Key& key) const= 0;

    /// Returns iterator by elements
    virtual iterator begin()= 0;
    virtual iterator end() = 0;

    virtual const_iterator begin() const = 0;
    virtual const_iterator end() const = 0;

    /// Returns count of replaced elements
    virtual size_t repSize() const = 0;

    /// Returns true if the element has been replaced
    virtual bool hasReplacedElement(const Key& key) const = 0;

    /// Return pointer to replaced value
    virtual ConstValuePtr getReplaced(const Key& key) const = 0;

    /// Returns iterator by replaced elements
    virtual iterator repBegin() = 0;
    virtual iterator repEnd() = 0;

    virtual const_iterator repBegin() const = 0;
    virtual const_iterator repEnd() const = 0;

    /// Adds observer of replacement event
    virtual void addRepObserver(ICacheReplacementObserver* observer) = 0;

    /// Removes observer of replacement event
    virtual void removeRepObserver(ICacheReplacementObserver* observer) = 0;

    /// Notifies replacement observers
    virtual void notifyRepObservers()const = 0;
};


#endif // ICACHE_H
