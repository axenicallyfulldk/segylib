#ifndef SEGYTRACEHEADER_H
#define SEGYTRACEHEADER_H

#include <map>

#include "field.h"

class SegyTraceHeader
{
public:

    enum Field {
        traceNumberLine,traceNumberReel,originalFieldRecordNumber,
        traceSeqNumberFR,sourceNumber,cdpNumber,traceSeqNumberCDP,traceIdCode,
        numVerticalSumTraces,numHorizontalStackedTraces,dataUse,distSrcRecv,
        recvElevation,surfElevation,depthBelowSurf,datumElevationRecv,
        datumElevationSrc,waterDepthSrc,waterDepthGroup,elevScalar,coordScalar,
        srcCoordX,srcCoordY,recvGroupCoordX,recvGroupCoordY,coordUnit,
        weatheringVelocity,subWeatheringVelocity,upholeTimeSrc,
        upholeTimeRsvGroup,srcStatCorrection,rcvGroupStatCorrection,
        totalStaticApplied,lagTimeA,lagTimeB,delayRecTime,muteTimeStart,
        muteTimeEnd,sampleCount,sampleInterval,gainType,instrGainCons,
        instrInitGain,correlated,sweepFrStart,sweepFrEnd,sweepLength,sweepType,
        sweepTraceTaperLengthStart,sweepTraceTaperLengthEnd,taperType,
        aliasFilterFr,aliasFilterSlope,notchFilterFr,notchFilterSlope,
        lowCutFr,highCutFr,lowCutSlope,highCutSlope,year,dayOfYear,hour,min,sec,
        timeCode,traceWeightFactor,geophoneRoll,geophoneFirstTrace,
        geophoneLastTrace,gapSize,travel,cdpX,cdpY,inlineNumber,xlineNumber,
        shotpointNumber,shotpointScalar,traceMeasurementUnit,
        transductionConstant,transductionUnit,devId,timeScalar,srcType,
        srcEnergyDirection,srcMeasurement,srcMeasurementUnit
    };

    using Mapping = std::map<Field, FieldMapping>;

    //If a new field has been added then check length
    static const std::array<Field, srcMeasurementUnit - traceNumberLine + 1> fields;


    SegyTraceHeader();

    int get(Field field)const;
    void set(Field field, int value);


    static const FieldInfo& getFieldInfo(Field field);

    static const std::map<Field, FieldMapping>& getDefaultMapping();

private:
    std::map<Field, int> fieldValue;

    static const std::map < Field, FieldInfo> _detailInfo;
    static const std::map < Field, FieldMapping> _defaultMapping;
};

#endif // SEGYTRACEHEADER_H
