#include "segytraceheader.h"

using namespace std;

const std::array<SegyTraceHeader::Field, SegyTraceHeader::srcMeasurementUnit-SegyTraceHeader::traceNumberLine + 1> SegyTraceHeader::fields = {
    SegyTraceHeader::traceNumberLine, SegyTraceHeader::traceNumberReel, SegyTraceHeader::originalFieldRecordNumber, SegyTraceHeader::traceSeqNumberFR,
    SegyTraceHeader::sourceNumber, SegyTraceHeader::cdpNumber, SegyTraceHeader::traceSeqNumberCDP, SegyTraceHeader::traceIdCode, SegyTraceHeader::numVerticalSumTraces,
    SegyTraceHeader::numHorizontalStackedTraces, SegyTraceHeader::dataUse, SegyTraceHeader::distSrcRecv, SegyTraceHeader::recvElevation,
    SegyTraceHeader::surfElevation, SegyTraceHeader::depthBelowSurf, SegyTraceHeader::datumElevationRecv, SegyTraceHeader::datumElevationSrc,
    SegyTraceHeader::waterDepthSrc, SegyTraceHeader::waterDepthGroup, SegyTraceHeader::elevScalar, SegyTraceHeader::coordScalar, SegyTraceHeader::srcCoordX,
    SegyTraceHeader::srcCoordY, SegyTraceHeader::recvGroupCoordX, SegyTraceHeader::recvGroupCoordY, SegyTraceHeader::coordUnit, SegyTraceHeader::weatheringVelocity,
    SegyTraceHeader::subWeatheringVelocity, SegyTraceHeader::upholeTimeSrc, SegyTraceHeader::upholeTimeRsvGroup, SegyTraceHeader::srcStatCorrection,
    SegyTraceHeader::rcvGroupStatCorrection, SegyTraceHeader::totalStaticApplied, SegyTraceHeader::lagTimeA, SegyTraceHeader::lagTimeB,
    SegyTraceHeader::delayRecTime, SegyTraceHeader::muteTimeStart, SegyTraceHeader::muteTimeEnd, SegyTraceHeader::sampleCount, SegyTraceHeader::sampleInterval,
    SegyTraceHeader::gainType, SegyTraceHeader::instrGainCons, SegyTraceHeader::instrInitGain, SegyTraceHeader::correlated, SegyTraceHeader::sweepFrStart,
    SegyTraceHeader::sweepFrEnd, SegyTraceHeader::sweepLength, SegyTraceHeader::sweepType, SegyTraceHeader::sweepTraceTaperLengthStart,
    SegyTraceHeader::sweepTraceTaperLengthEnd, SegyTraceHeader::taperType, SegyTraceHeader::aliasFilterFr, SegyTraceHeader::aliasFilterSlope,
    SegyTraceHeader::notchFilterFr, SegyTraceHeader::notchFilterSlope, SegyTraceHeader::lowCutFr, SegyTraceHeader::highCutFr, SegyTraceHeader::lowCutSlope,
    SegyTraceHeader::highCutSlope, SegyTraceHeader::year, SegyTraceHeader::dayOfYear, SegyTraceHeader::hour, SegyTraceHeader::min, SegyTraceHeader::sec,
    SegyTraceHeader::timeCode, SegyTraceHeader::traceWeightFactor, SegyTraceHeader::geophoneRoll, SegyTraceHeader::geophoneFirstTrace,
    SegyTraceHeader::geophoneLastTrace, SegyTraceHeader::gapSize, SegyTraceHeader::travel, SegyTraceHeader::cdpX, SegyTraceHeader::cdpY, SegyTraceHeader::inlineNumber,
    SegyTraceHeader::xlineNumber, SegyTraceHeader::shotpointNumber, SegyTraceHeader::shotpointScalar, SegyTraceHeader::traceMeasurementUnit,
    SegyTraceHeader::transductionConstant, SegyTraceHeader::transductionUnit, SegyTraceHeader::devId, SegyTraceHeader::timeScalar, SegyTraceHeader::srcType,
    SegyTraceHeader::srcEnergyDirection, SegyTraceHeader::srcMeasurement, SegyTraceHeader::srcMeasurementUnit
};

SegyTraceHeader::SegyTraceHeader()
{
    for(auto field: fields){
        fieldValue[field] = 0;
    }
}


int SegyTraceHeader::get(Field field) const{
    auto it = fieldValue.find(field);
    if(it != fieldValue.end()){
        return it->second;
    }

    return -1;
}

void SegyTraceHeader::set(Field field, int value){
    fieldValue[field] = value;
}

const FieldInfo &SegyTraceHeader::getFieldInfo(SegyTraceHeader::Field field){
    return _detailInfo.at(field);
}

const std::map<SegyTraceHeader::Field, FieldMapping> &SegyTraceHeader::getDefaultMapping(){
    return _defaultMapping;
}

static map<SegyTraceHeader::Field, FieldInfo> initDetailInfo(){
    map<SegyTraceHeader::Field, FieldInfo> mmap;
    mmap.insert(make_pair(SegyTraceHeader::traceNumberLine, FieldInfo("traceNumberLine", "Trace sequence within line","Numbers continue to increase if the same line continues across multiple SEGY files (Highly recommended for all types of data)")));
    mmap.insert(make_pair(SegyTraceHeader::traceNumberReel, FieldInfo("traceNumberReel", "Trace sequence number within SEGY file","Each file starts with trace sequence one")));
    mmap.insert(make_pair(SegyTraceHeader::originalFieldRecordNumber, FieldInfo("originalFieldRecordNumber", "Original field record number")));
    mmap.insert(make_pair(SegyTraceHeader::traceSeqNumberFR, FieldInfo("traceSeqNumberFR", "Trace number within the origina field record")));
    mmap.insert(make_pair(SegyTraceHeader::sourceNumber, FieldInfo("sourceNumber", "Energy source point number","Used when more than one record occurs at the same effective surface location. It is recommended that the new entry defined in Trace Header bytes 197-202 be used for shotpoint number.")));
    mmap.insert(make_pair(SegyTraceHeader::cdpNumber, FieldInfo("cdpNumber", "Ensemble number","Ensemble number (i.e. CDP, CMP, CRP, etc)")));
    mmap.insert(make_pair(SegyTraceHeader::traceSeqNumberCDP, FieldInfo("traceSeqNumberCDP", "Trace number within the ensemble","Each ensemble starts with trace number one")));
    mmap.insert(make_pair(SegyTraceHeader::traceIdCode, FieldInfo("traceIdCode", "Trace identification code","-1=Other\n0=Unknown\n1=Seismic data\n2=Dead\n3=Dummy\n4=Time break\n5=Uphole\n6=Sweep\n7=Timing\n8=Waterbreak\n9=Near-field hun signature\n10=Far-field gun signature\n11=Seismic pressure sensor\n12=Multicomponent seismic sensor - Vertical component\n13=Multicomponent seismic sensor - Cross-line component\n14=Multicomponent seismic sensor - In-line component\n15=Rotated multicomponent seismic sensor - Vertical component\n16=Rotated multicomponent seismic sensor - Transverse component\n17=Rotated multicomponent seismic sensor - Radial component\n18=Vibrator reaction mass\n19=Vibrator baseplate\n20=Vibrator estimated ground force\n21=Vibrator reference\n22=Time-velocity pairs")));
    mmap.insert(make_pair(SegyTraceHeader::numVerticalSumTraces, FieldInfo("numVerticalSumTraces", "Number of vertically summed traces","Number of vertically summed traces yielding this trace (1 is one trace, 2 is two summed traces, etc.)")));
    mmap.insert(make_pair(SegyTraceHeader::numHorizontalStackedTraces, FieldInfo("numHorizontalStackedTraces", "Number of horizontally stacked traces","Number of horizontally stacked traces yielding this trace (1 is one trace, 2 is two stacked traces, etc.)")));
    mmap.insert(make_pair(SegyTraceHeader::dataUse, FieldInfo("dataUse", "Data use","1=Production\n2=Test")));
    mmap.insert(make_pair(SegyTraceHeader::distSrcRecv, FieldInfo("distSrcRecv", "Distance between recv and source groups","Distance from center of the source point to the center of the receiver group (negative if opposite to direction in which line is shot)")));
    mmap.insert(make_pair(SegyTraceHeader::recvElevation, FieldInfo("recvElevation", "Receiver group elevation","All elevations above the Vertucal datum are positive and below are negative")));
    mmap.insert(make_pair(SegyTraceHeader::surfElevation, FieldInfo("surfElevation", "Surface elevation at source")));
    mmap.insert(make_pair(SegyTraceHeader::depthBelowSurf, FieldInfo("depthBelowSurf", "Source depth below surface")));
    mmap.insert(make_pair(SegyTraceHeader::datumElevationRecv, FieldInfo("datumElevationRecv", "Datum elevation at receiver group")));
    mmap.insert(make_pair(SegyTraceHeader::datumElevationSrc, FieldInfo("datumElevationSrc", "Datum elevation at source")));
    mmap.insert(make_pair(SegyTraceHeader::waterDepthSrc, FieldInfo("waterDepthSrc", "Water depth at source")));
    mmap.insert(make_pair(SegyTraceHeader::waterDepthGroup, FieldInfo("waterDepthGroup", "Water depth at group")));
    mmap.insert(make_pair(SegyTraceHeader::elevScalar, FieldInfo("elevScalar", "Elevation scalar","Scalar to be applied to all elevations and depths specified in Trace Header bytes 41-68 to give the real value. Scalar=1,+10,+100,+1000, or +10000. If positive, scalar is used as a multiplier; if negative, scalar is used as a divisor.")));
    mmap.insert(make_pair(SegyTraceHeader::coordScalar, FieldInfo("coordScalar", "Coordinate scalar","Scalar to be applied to all coordinates specified in Trace Header bytes 73-88 and to bytes Trace Header 181-188 to give the real value. Scalar=1,+10,+100,+1000, or +10000. If positive, scalar is used as a multiplier; if negative, scalar is used as divisor.")));
    mmap.insert(make_pair(SegyTraceHeader::srcCoordX, FieldInfo("srcCoordX", "Source coordinate - X")));
    mmap.insert(make_pair(SegyTraceHeader::srcCoordY, FieldInfo("srcCoordY", "Source coordinate - Y")));
    mmap.insert(make_pair(SegyTraceHeader::recvGroupCoordX, FieldInfo("recvGroupCoordX", "Group coordinate - X")));
    mmap.insert(make_pair(SegyTraceHeader::recvGroupCoordY, FieldInfo("recvGroupCoordY", "Group coordinate - Y")));
    mmap.insert(make_pair(SegyTraceHeader::coordUnit, FieldInfo("coordUnit", "Coordinate units","1=Length (meters or feet)\n2=Seconds of arc\n3=Decimal degrees\n4=Degrees, minutes, seconds (DMS)")));
    mmap.insert(make_pair(SegyTraceHeader::weatheringVelocity, FieldInfo("weatheringVelocity", "Weathering velocity")));
    mmap.insert(make_pair(SegyTraceHeader::subWeatheringVelocity, FieldInfo("subWeatheringVelocity", "Subweathering velocity")));
    mmap.insert(make_pair(SegyTraceHeader::upholeTimeSrc, FieldInfo("upholeTimeSrc", "Uphole time at source in milliseconds")));
    mmap.insert(make_pair(SegyTraceHeader::upholeTimeRsvGroup, FieldInfo("upholeTimeRsvGroup", "Uphole time at group in milliseconds")));
    mmap.insert(make_pair(SegyTraceHeader::srcStatCorrection, FieldInfo("srcStatCorrection", "Source static correction in milliseconds")));
    mmap.insert(make_pair(SegyTraceHeader::rcvGroupStatCorrection, FieldInfo("rcvGroupStatCorrection", "Group static correction in milliseconds")));
    mmap.insert(make_pair(SegyTraceHeader::totalStaticApplied, FieldInfo("totalStaticApplied", "Total static applied in milliseconds","Zero if no static has been applied")));
    mmap.insert(make_pair(SegyTraceHeader::lagTimeA, FieldInfo("lagTimeA", "Lag time A","Time in milliseconds beween end of 240-byte trace identification header and time break. The value is positive if time break occurs after the end of header; negative if time break occurs before the end of header. Time break is defined as the intiation pulse that may be recorded on an auxiliary trace of as otherwise specified by the recording system.")));
    mmap.insert(make_pair(SegyTraceHeader::lagTimeB, FieldInfo("lagTimeB", "Lag time B","Time in milliseconds between time break and the initiation time of the energy source. May be positive or negative")));
    mmap.insert(make_pair(SegyTraceHeader::delayRecTime, FieldInfo("delayRecTime", "Delay recording time","Time in milliseconds beween initiation time of energy source and the time when recording of data samples begins. In SEGY rev 0 this entry was intended for deep-water work if data recording does not start at zero time. The entry can be negative to accommodate negative start times (i.e. data recorded before time zero, presumably as a result of static application to the data trace). If a non-zero value (negative or positive) is recorded in this entry, a comment to that effect should appear in the Textul File Header.")));
    mmap.insert(make_pair(SegyTraceHeader::muteTimeStart, FieldInfo("muteTimeStart", "Start mute time","Start mute time in milliseconds")));
    mmap.insert(make_pair(SegyTraceHeader::muteTimeEnd, FieldInfo("muteTimeEnd", "End mute time","End mute time in milliseconds")));
    mmap.insert(make_pair(SegyTraceHeader::sampleCount, FieldInfo("sampleCount", "Number of samples in this trace")));
    mmap.insert(make_pair(SegyTraceHeader::sampleInterval, FieldInfo("sampleInterval", "Sample interval","Sample interval in microseconds for this trace. The number of bytes in a trace record must be consistent with the number of samples written in the trace header. This is important for all recording media; but it is particularly crucial for the correct processing of SEGY data in disk files.\nIf the fixed length trace flag in bytes 3503-3504 of the Binary File Header is set, the sample interval and number of samples in every trace in the SEGY file must be the save as the values recorded in the Binary File Header. If the fixed length trace flag is not set, the sample interval and number of samples may vary from trace to trace.")));
    mmap.insert(make_pair(SegyTraceHeader::gainType, FieldInfo("gainType", "Gain type of field instrumets","1=fixed\n2=binary\n3=floating point\n4...N=optional use")));
    mmap.insert(make_pair(SegyTraceHeader::instrGainCons, FieldInfo("instrGainCons", "Instrument gain constant","Instrument gain constant in dB")));
    mmap.insert(make_pair(SegyTraceHeader::instrInitGain, FieldInfo("instrInitGain", "Instrument early or initial gain","Instrument early or initial gain in dB")));
    mmap.insert(make_pair(SegyTraceHeader::correlated, FieldInfo("correlated", "Correlated","1=no\n2=yes")));
    mmap.insert(make_pair(SegyTraceHeader::sweepFrStart, FieldInfo("sweepFrStart", "Seep frequency at start","Seep frequency at start in Hz")));
    mmap.insert(make_pair(SegyTraceHeader::sweepFrEnd, FieldInfo("sweepFrEnd", "Seep frequency at end","Seep frequency at start in Hz")));
    mmap.insert(make_pair(SegyTraceHeader::sweepLength, FieldInfo("sweepLength", "Sweep length","Sweep length in milliseconds")));
    mmap.insert(make_pair(SegyTraceHeader::sweepType, FieldInfo("sweepType", "Sweep type","1=linear\n2=parabolic\n3=exponential\4=other")));
    mmap.insert(make_pair(SegyTraceHeader::sweepTraceTaperLengthStart, FieldInfo("sweepTraceTaperLengthStart", "Sweep trace taper length at start","Sweep trace taper length at start in milliseconds")));
    mmap.insert(make_pair(SegyTraceHeader::sweepTraceTaperLengthEnd, FieldInfo("sweepTraceTaperLengthEnd", "Sweep trace taper length at end","Sweep trace taper length at end in milliseconds")));
    mmap.insert(make_pair(SegyTraceHeader::taperType, FieldInfo("taperType", "Taper type","1=linear\n2=cos^2\n3=other")));
    mmap.insert(make_pair(SegyTraceHeader::aliasFilterFr, FieldInfo("aliasFilterFr", "Alias filter frequency","Alias filter frequency (Hz), if used")));
    mmap.insert(make_pair(SegyTraceHeader::aliasFilterSlope, FieldInfo("aliasFilterSlope", "Alias filter slope","Alias filter slope (dB/octave)")));
    mmap.insert(make_pair(SegyTraceHeader::notchFilterFr, FieldInfo("notchFilterFr", "Notch filter frequency","Notch filter frequency (Hz), if used")));
    mmap.insert(make_pair(SegyTraceHeader::notchFilterSlope, FieldInfo("notchFilterSlope", "Notch fileter slope","Notch fileter slope (dB/octave)")));
    mmap.insert(make_pair(SegyTraceHeader::lowCutFr, FieldInfo("lowCutFr", "Low-cut frequency","Low-cut frequency (Hz), if used")));
    mmap.insert(make_pair(SegyTraceHeader::highCutFr, FieldInfo("highCutFr", "High-cut frequency","High-cut frequency (Hz), if used")));
    mmap.insert(make_pair(SegyTraceHeader::lowCutSlope, FieldInfo("lowCutSlope", "Low-cut slope","Low-cut slope (db/octave)")));
    mmap.insert(make_pair(SegyTraceHeader::highCutSlope, FieldInfo("highCutSlope", "High-cut slope","High-cut slope (db/octave)")));
    mmap.insert(make_pair(SegyTraceHeader::year, FieldInfo("year", "Year data recorded","The 1975 standard is unclear as to whether this sould be recorded as a 2-digit or a 4-digit year and both have been used. For SEGY revisions beyond rev 0, the year should be recorded as the complete 4-digit Gregorian calendar year (i.e. the year 2001 should be recorded as 2001_10 (7D1_16))")));
    mmap.insert(make_pair(SegyTraceHeader::dayOfYear, FieldInfo("dayOfYear", "Day of year","Julian day for GMT and UTC time basis")));
    mmap.insert(make_pair(SegyTraceHeader::hour, FieldInfo("hour", "Hour of daty","24 hour clock")));
    mmap.insert(make_pair(SegyTraceHeader::min, FieldInfo("min", "Minute of hour")));
    mmap.insert(make_pair(SegyTraceHeader::sec, FieldInfo("sec", "Second of minute")));
    mmap.insert(make_pair(SegyTraceHeader::timeCode, FieldInfo("timeCode", "Time basis code","1=Local\n2=GMT (Greenwich Mean Time)\n3=Other, should be explained in a user defined stanza in the Extended Textual File Header\n4=UTC (Coordinated Universal Time)")));
    mmap.insert(make_pair(SegyTraceHeader::traceWeightFactor, FieldInfo("traceWeightFactor", "Trace weighting factor","Defined as 2^(-N) volts for the least significant bit")));
    mmap.insert(make_pair(SegyTraceHeader::geophoneRoll, FieldInfo("geophoneRoll", "Geophone group number of roll switch position one")));
    mmap.insert(make_pair(SegyTraceHeader::geophoneFirstTrace, FieldInfo("geophoneFirstTrace", "Geophone group number of trace number one within origianl field record")));
    mmap.insert(make_pair(SegyTraceHeader::geophoneLastTrace, FieldInfo("geophoneLastTrace", "Grophone group number of last trace within original field record")));
    mmap.insert(make_pair(SegyTraceHeader::gapSize, FieldInfo("gapSize", "Gap size","Total number of groups dropped")));
    mmap.insert(make_pair(SegyTraceHeader::travel, FieldInfo("travel", "Over travel associated with tape at beginning or end of line","1=down (or behind)\n2=up (or ahead)")));
    mmap.insert(make_pair(SegyTraceHeader::cdpX, FieldInfo("cdpX", "X coordinate of ensemble position", "X coordinate of ensemble (CDP) position of this trace (scalar in Trace Header bytes 71-72 applies). The coordinate reference system should be identified through an extended header Location Data stanza")));
    mmap.insert(make_pair(SegyTraceHeader::cdpY, FieldInfo("cdpY", "Y coordinate of ensemble position", "X coordinate of ensemble (CDP) position of this trace (scalar in Trace Header bytes 71-72 applies). The coordinate reference system should be identified through an extended header Location Data stanza")));
    mmap.insert(make_pair(SegyTraceHeader::inlineNumber, FieldInfo("inlineNumber", "Inline number","For 3-D poststack data, this field should be used for the in-line number. If one in-line per SEGY file is being recorded, this value should be the same for all traces in the file and the same value will be recorded in bytes 3205-3208 of the Binary File Header.")));
    mmap.insert(make_pair(SegyTraceHeader::xlineNumber, FieldInfo("xlineNumber", "Cross-line number","For 3-D poststack data, this field should be used for the cross-line number. This weill typically be the same value as the ensemble (CDP) number in Trace Header bytes 21-24, but this does not have to be the case.")));
    mmap.insert(make_pair(SegyTraceHeader::shotpointNumber, FieldInfo("shotpointNumber", "Shotpoint number","This is probably only applicable to 2-D poststack data. Note that it is assumed that the shotpoint number refers to the source location nearest to the ensemble (CDP) location for a particular trace . If this is not the case, there should be a commnet in the Textual File Header explaining what the shotpoint number actually refers to.")));
    mmap.insert(make_pair(SegyTraceHeader::shotpointScalar, FieldInfo("shotpointScalar", "Shotpoint scalar","Scalar to be applied to the shotpoint number in Trace Header bytes 197-200 to give the real value. If positive, scalar is used as a multiplier; if negative as a divisor; if zero the shotpoint number is not scaled (i.e. it is an integer. A typical value will be -10,allowing shotpoint numbers with one decimal digit to the right of the decimal point)")));
    mmap.insert(make_pair(SegyTraceHeader::traceMeasurementUnit, FieldInfo("traceMeasurementUnit", "Trace value measurement unit","-1=Other (should be described in Data Sample Measurement Units Stanza)\n0=Unknown\n1=Padcal (Pa)\n2=Volts (v)\n3=Millivolts (mV)\n4=Amperes (A)\n5=Meters (m)\n6=Meters per second (m/s)\n7=Meters per second squared (m/s^2)\n8=Newton (N)\n9=Watt (W)")));
    mmap.insert(make_pair(SegyTraceHeader::transductionConstant, FieldInfo("transductionConstant", "Transduction Constant","The miltiplicative constant used to convert the Data Trace samples to the Transduction Units (specified in Trace Header bytes 211-212). The constant is encoded as a four-byte, two's complement integer (bytes 205-208) which is the mantissa and a two-byte, two's complement integer (bytes 209-210) which is the power of ten exponent (i.e. Bytes 205-208 * 10**Bytes 209-210).")));
    mmap.insert(make_pair(SegyTraceHeader::transductionUnit, FieldInfo("transductionUnit", "Transduction Units","The unit of measurement of the Data Trace samples after they have been multiplied by the Transduction Constant specified in Trace Header bytes 205-210.\n-1=Other (should be described in Data Sample Measurement Units Stanza)\n0=Unknown\n1=Padcal (Pa)\n2=Volts (v)\n3=Millivolts (mV)\n4=Amperes (A)\n5=Meters (m)\n6=Meters per second (m/s)\n7=Meters per second squared (m/s^2)\n8=Newton (N)\n9=Watt (W)")));
    mmap.insert(make_pair(SegyTraceHeader::devId, FieldInfo("devId", "Device/Trace Identifier","The unit number or id number of the device associated with the Data Trace (i.e. 4368 for vibrator serial number 4368 or 20316 for gun 16 on string 3 on vessel 2). This field allow traces to be associated across trace ensembles independently of the trace number.")));
    mmap.insert(make_pair(SegyTraceHeader::timeScalar, FieldInfo("timeScalar", "Scalar to be applied to times","Scalar to be applied to times specified in Trace Header bytes 95-114 to give the true time value in milliseconds. Scalar=1,+10,+100,+1000, or +10000. If positive, scalar is used as a pultiplier; if negative, scalar is used as divisor. A value of zero is assumed to be a scalar value of 1.")));
    mmap.insert(make_pair(SegyTraceHeader::srcType, FieldInfo("srcType", "Source Type/Orientation","Defines the type and the orientation of the energy source. The terms vertical, cross-line an in-line refer to the threee axes of an orthogonal coordinate system. The absolute azimuthal orientation o the coordinate system axes can be defined in the Bin Grid Definition Stanza.\n-1 to -n = Other (should be described in Source Type/Orientation stanza)\n0=Unknown\n1=Vibratory - Vertical orientation\n2=Vibratory - Cross-line orentation\n3=Vibratory - In-line orentation\n4=Impulsive - Veretical orentation\n5=Impulsive - Cross-line orientation\n6=impulsive - In-line orientation\n7=Distributed Impulsive - Vertical orentation\n8=Distributed Impulsive - Cross-line orientation\n9=Distributed Impulsive - In-line orentation")));
    mmap.insert(make_pair(SegyTraceHeader::srcEnergyDirection, FieldInfo("srcEnergyDirection", "Source Energy Direction with respect ot the source orientation","The positive orientation direction is defined in Bytes 217-218 of the Trace HEader. The energy direction is encoded in tenths of degrees (i.e. 347.8 is encoded as 3478)")));
    mmap.insert(make_pair(SegyTraceHeader::srcMeasurement, FieldInfo("srcMeasurement", "Sourse Measurement","Describes the source effort used to generate the trace. The measurement can be simple, qualitative measurements such as the total weight of explosive used or the peak air gun pressure or the number of vibrators times the sweep duration. Although these simple measurements are acceptable, it is preferable to use ture measurement units of energy or work.\nThe constant is encoded as a four-byte, two's complement integer (bytes 225-228) which is the mantissa and a two-byte, two's complement integer (bytes 209-230) which is the power of ten exponent (i.e. Bytes 225-228 * 10 **Bytes 229-230)")));
    mmap.insert(make_pair(SegyTraceHeader::srcMeasurementUnit, FieldInfo("srcMeasurementUnit", "Source Measurement Unit","The unit used for the Source Measurement, Trace header bytes 225-230.\n-1=Other (should be described in Source Measurement Unit stanza)\n0=Unknown\n1=Joule (J)\n2=Kilowatt (kW)\n3=Pascal (Pa)\n4=Bar (Bar)\n4=Bar-meter (Bar-m)\n5=Newton (N)\n6=Kilograms (kg)")));
    return mmap;
}

const map <SegyTraceHeader::Field, FieldInfo> SegyTraceHeader::_detailInfo = initDetailInfo();



static std::map < SegyTraceHeader::Field, FieldMapping> initMapperInfo(){
    std::map<SegyTraceHeader::Field, FieldMapping> mmap;
    mmap[SegyTraceHeader::traceNumberLine] = FieldMapping(0, 4);
    mmap[SegyTraceHeader::traceNumberReel] = FieldMapping(4, 4);
    mmap[SegyTraceHeader::originalFieldRecordNumber] = FieldMapping(8, 4);
    mmap[SegyTraceHeader::traceSeqNumberFR] = FieldMapping(12, 4);
    mmap[SegyTraceHeader::sourceNumber] = FieldMapping(16, 4);
    mmap[SegyTraceHeader::cdpNumber] = FieldMapping(20, 4);
    mmap[SegyTraceHeader::traceSeqNumberCDP] = FieldMapping(24, 4);
    mmap[SegyTraceHeader::traceIdCode] = FieldMapping(28, 2);
    mmap[SegyTraceHeader::numVerticalSumTraces] = FieldMapping(30, 2);
    mmap[SegyTraceHeader::numHorizontalStackedTraces] = FieldMapping(32, 2);
    mmap[SegyTraceHeader::dataUse] = FieldMapping(34, 2);
    mmap[SegyTraceHeader::distSrcRecv] = FieldMapping(36, 4);
    mmap[SegyTraceHeader::recvElevation] = FieldMapping(40, 4);
    mmap[SegyTraceHeader::surfElevation] = FieldMapping(44, 4);
    mmap[SegyTraceHeader::depthBelowSurf] = FieldMapping(48, 4);
    mmap[SegyTraceHeader::datumElevationRecv] = FieldMapping(52, 4);
    mmap[SegyTraceHeader::datumElevationSrc] = FieldMapping(56, 4);
    mmap[SegyTraceHeader::waterDepthSrc] = FieldMapping(60, 4);
    mmap[SegyTraceHeader::waterDepthGroup] = FieldMapping(64, 4);
    mmap[SegyTraceHeader::elevScalar] = FieldMapping(68, 2);
    mmap[SegyTraceHeader::coordScalar] = FieldMapping(70, 2);
    mmap[SegyTraceHeader::srcCoordX] = FieldMapping(72, 4);
    mmap[SegyTraceHeader::srcCoordY] = FieldMapping(76, 4);
    mmap[SegyTraceHeader::recvGroupCoordX] = FieldMapping(80, 4);
    mmap[SegyTraceHeader::recvGroupCoordY] = FieldMapping(84, 4);
    mmap[SegyTraceHeader::coordUnit] = FieldMapping(88, 2);
    mmap[SegyTraceHeader::weatheringVelocity] = FieldMapping(90, 2);
    mmap[SegyTraceHeader::subWeatheringVelocity] = FieldMapping(92, 2);
    mmap[SegyTraceHeader::upholeTimeSrc] = FieldMapping(94, 2);
    mmap[SegyTraceHeader::upholeTimeRsvGroup] = FieldMapping(96, 2);
    mmap[SegyTraceHeader::srcStatCorrection] = FieldMapping(98, 2);
    mmap[SegyTraceHeader::rcvGroupStatCorrection] = FieldMapping(100, 2);
    mmap[SegyTraceHeader::totalStaticApplied] = FieldMapping(102, 2);
    mmap[SegyTraceHeader::lagTimeA] = FieldMapping(104, 2);
    mmap[SegyTraceHeader::lagTimeB] = FieldMapping(106, 2);
    mmap[SegyTraceHeader::delayRecTime] = FieldMapping(108, 2);
    mmap[SegyTraceHeader::muteTimeStart] = FieldMapping(110, 2);
    mmap[SegyTraceHeader::muteTimeEnd] = FieldMapping(112, 2);
    mmap[SegyTraceHeader::sampleCount] = FieldMapping(114, 2);
    mmap[SegyTraceHeader::sampleInterval] = FieldMapping(116, 2);
    mmap[SegyTraceHeader::gainType] = FieldMapping(118, 2);
    mmap[SegyTraceHeader::instrGainCons] = FieldMapping(120, 2);
    mmap[SegyTraceHeader::instrInitGain] = FieldMapping(122, 2);
    mmap[SegyTraceHeader::correlated] = FieldMapping(124, 2);
    mmap[SegyTraceHeader::sweepFrStart] = FieldMapping(126, 2);
    mmap[SegyTraceHeader::sweepFrEnd] = FieldMapping(128, 2);
    mmap[SegyTraceHeader::sweepLength] = FieldMapping(130, 2);
    mmap[SegyTraceHeader::sweepType] = FieldMapping(132, 2);
    mmap[SegyTraceHeader::sweepTraceTaperLengthStart] = FieldMapping(134, 2);
    mmap[SegyTraceHeader::sweepTraceTaperLengthEnd] = FieldMapping(136, 2);
    mmap[SegyTraceHeader::taperType] = FieldMapping(138, 2);
    mmap[SegyTraceHeader::aliasFilterFr] = FieldMapping(140, 2);
    mmap[SegyTraceHeader::aliasFilterSlope] = FieldMapping(142, 2);
    mmap[SegyTraceHeader::notchFilterFr] = FieldMapping(144, 2);
    mmap[SegyTraceHeader::notchFilterSlope] = FieldMapping(146, 2);
    mmap[SegyTraceHeader::lowCutFr] = FieldMapping(148, 2);
    mmap[SegyTraceHeader::highCutFr] = FieldMapping(150, 2);
    mmap[SegyTraceHeader::lowCutSlope] = FieldMapping(152, 2);
    mmap[SegyTraceHeader::highCutSlope] = FieldMapping(154, 2);
    mmap[SegyTraceHeader::year] = FieldMapping(156, 2);
    mmap[SegyTraceHeader::dayOfYear] = FieldMapping(158, 2);
    mmap[SegyTraceHeader::hour] = FieldMapping(160, 2);
    mmap[SegyTraceHeader::min] = FieldMapping(162, 2);
    mmap[SegyTraceHeader::sec] = FieldMapping(164, 2);
    mmap[SegyTraceHeader::timeCode] = FieldMapping(166, 2);
    mmap[SegyTraceHeader::traceWeightFactor] = FieldMapping(168, 2);
    mmap[SegyTraceHeader::geophoneRoll] = FieldMapping(170, 2);
    mmap[SegyTraceHeader::geophoneFirstTrace] = FieldMapping(172, 2);
    mmap[SegyTraceHeader::geophoneLastTrace] = FieldMapping(174, 2);
    mmap[SegyTraceHeader::gapSize] = FieldMapping(176, 2);
    mmap[SegyTraceHeader::travel] = FieldMapping(178, 2);
    mmap[SegyTraceHeader::cdpX] = FieldMapping(180, 4);
    mmap[SegyTraceHeader::cdpY] = FieldMapping(184, 4);
    mmap[SegyTraceHeader::inlineNumber] = FieldMapping(188, 4);
    mmap[SegyTraceHeader::xlineNumber] = FieldMapping(192, 4);
    mmap[SegyTraceHeader::shotpointNumber] = FieldMapping(196, 4);
    mmap[SegyTraceHeader::shotpointScalar] = FieldMapping(200, 2);
    mmap[SegyTraceHeader::traceMeasurementUnit] = FieldMapping(202, 2);
    mmap[SegyTraceHeader::transductionConstant] = FieldMapping(204, 6);
    mmap[SegyTraceHeader::transductionUnit] = FieldMapping(210, 2);
    mmap[SegyTraceHeader::devId] = FieldMapping(212, 2);
    mmap[SegyTraceHeader::timeScalar] = FieldMapping(214, 2);
    mmap[SegyTraceHeader::srcType] = FieldMapping(216, 2);
    mmap[SegyTraceHeader::srcEnergyDirection] = FieldMapping(218, 6);
    mmap[SegyTraceHeader::srcMeasurement] = FieldMapping(224, 6);
    mmap[SegyTraceHeader::srcMeasurementUnit] = FieldMapping(230, 2);
    return mmap;
}

const std::map < SegyTraceHeader::Field, FieldMapping> SegyTraceHeader::_defaultMapping = initMapperInfo();
