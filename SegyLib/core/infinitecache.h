#ifndef INFINITECACHE_H
#define INFINITECACHE_H

#include "icache.h"

#include <map>
#include <list>

/// Class that implements infinite capacity cache
template<class Key, class Value>
class InfiniteCache:public ICache<Key, Value>{
public:
    typedef std::shared_ptr<Value> ValuePtr;
    typedef std::shared_ptr<const Value> ConstValuePtr;

private:
    std::map<Key, ValuePtr> _data;
    std::list<ICacheReplacementObserver*> _observers;

    template<bool isConstant>
    class InfiniteIterator: public ICache<Key, Value>::template IIteratorImpl<isConstant>{
    private:
        using BaseType = typename ICache<Key, Value>::template IIteratorImpl<isConstant>;
        using MapIteratorType = std::conditional_t<isConstant, typename std::map<Key, ValuePtr>::const_iterator, typename std::map<Key, ValuePtr>::iterator>;
        using ValPtr = typename BaseType::ValuePtr;

        MapIteratorType _it;

        InfiniteIterator(const InfiniteIterator<isConstant>& it){
            _it = it._it;
        }

        InfiniteIterator(const MapIteratorType& it){
            _it = it;
        }

        friend class InfiniteCache;
        friend class InfiniteIterator<!isConstant>;

    public:
        virtual const Key& getKey()const{
            return _it->first;
        }

        virtual ValPtr getValue()const{
            return _it->second;
        }


        virtual bool operator==(const BaseType& it)const{
             auto& iit = dynamic_cast<const InfiniteIterator<isConstant>&>(it);
             return _it == iit._it;
        }

        virtual InfiniteIterator<isConstant>* clone() const{
            return new InfiniteIterator<isConstant>(*this);
        }

        const InfiniteIterator<isConstant> &operator ++(){
            ++_it;
            return *this;
        }
    };

public:

    using iterator = typename ICache<Key, Value>::iterator;
    using const_iterator = typename ICache<Key, Value>::const_iterator;

    InfiniteCache(){}

    virtual size_t getMaxSize() const {
        return std::numeric_limits<size_t>::max();
    }

    virtual void setMaxSize(size_t s){
        return;
    }

    virtual void put(const Key& key, ValuePtr value){
        _data[key] = value;
    }

    virtual std::shared_ptr<Value> remove(const Key& key){
        std::shared_ptr<Value> val;
        auto it = _data.find(key);
        if (it != _data.end()){
            val = it->second;
            _data.erase(it);
        }
        return val;
    }

    virtual void clear(){
        _data.clear();
    }

    virtual size_t size() const{
        return _data.size();
    }

    virtual bool hasElement(const Key& key)const{
        return _data.find(key) != _data.end();
    }

    virtual ValuePtr get(const Key& key){
        ValuePtr val;

        auto it = _data.find(key);
        if (it != _data.end())
            val = it->second;

        return val;
    }

    virtual ConstValuePtr get(const Key& key)const{
        ConstValuePtr val;

        auto it = _data.find(key);
        if (it != _data.end())
            val = it->second;

        return val;
    }

    virtual iterator begin(){
        auto it = new InfiniteIterator<false>(_data.begin());
        return ICache<Key,Value>::createIterator(it);
    }
    virtual iterator end(){
        auto it = new InfiniteIterator<false>(_data.end());
        return ICache<Key,Value>::createIterator(it);
    }

    virtual const_iterator begin() const{
        auto it = new InfiniteIterator<true>(_data.begin());
        return ICache<Key,Value>::createIterator(it);
    }
    virtual const_iterator end() const{
        auto it = new InfiniteIterator<true>(_data.end());
        return ICache<Key,Value>::createIterator(it);
    }


    virtual size_t repSize() const{
        return 0;
    }

    virtual bool hasReplacedElement(const Key& key) const{
        return false;
    }

    virtual ConstValuePtr getReplaced(const Key& key) const{
        return ConstValuePtr();
    }

    virtual iterator repBegin(){
        auto it = new InfiniteIterator<false>(_data.end());
        return ICache<Key,Value>::createIterator(it);
    }
    virtual iterator repEnd(){
        auto it = new InfiniteIterator<false>(_data.end());
        return ICache<Key,Value>::createIterator(it);
    }

    virtual const_iterator repBegin() const{
        auto it = new InfiniteIterator<true>(_data.end());
        return ICache<Key,Value>::createIterator(it);
    }
    virtual const_iterator repEnd() const{
        auto it = new InfiniteIterator<true>(_data.end());
        return ICache<Key,Value>::createIterator(it);
    }

    virtual void addRepObserver(ICacheReplacementObserver* observer){
        auto it = std::find(_observers.begin(), _observers.end(), observer);
        if (it == _observers.end())
            _observers.push_back(observer);
    }

    virtual void removeRepObserver(ICacheReplacementObserver* observer) {
        auto it = std::find(_observers.begin(), _observers.end(), observer);
        _observers.erase(it);
    }

    virtual void notifyRepObservers()const{
        for (auto it= _observers.begin(); it != _observers.end(); ++it){
            (*it)->handleReplacement();
        }
    }
};


#endif // INFINITECACHE_H
