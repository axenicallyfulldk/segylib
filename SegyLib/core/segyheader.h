#ifndef SEGYHEADER_H
#define SEGYHEADER_H

#include <array>
#include <map>

#include "field.h"


class SegyHeader
{
public:

    enum Field {
        jobId, lineNumber, reelNumber, numTracesPerRecord,
        numOfAxualiryTracesPerRecord, sampleInterval, sampleIntervalOrigin,
        sampleCount, sampleCountOrigin, type, cdpFold, traceSorting,
        verticalSum, sweepFrStart, sweepFrEnd, sweepLength, sweepType,
        traceNumOfSweepChannel, sweepTaperLengthStart, sweepTaperLengthEnd,
        taperType, corrDataTraces, binGainRecovery, ampRecoveryMethod,
        measurementSys, impSignal, polarityCode, segyFormat, fixedLength
    };

    using Mapping = std::map<Field, FieldMapping>;

    //If a new field has been added then check length
    static const std::array<Field, fixedLength - jobId + 1> fields;

    SegyHeader();

    int get(Field field) const;
    void set(Field field, int value);


    static const FieldInfo& getFieldInfo(Field field);

    static const std::map<Field, FieldMapping>& getDefaultMapping();

private:
    std::map<Field, int> fieldValue;

    static const std::map < Field, FieldInfo> _detailInfo;
    static const std::map < Field, FieldMapping> _defaultMapping;
};

#endif // SEGYHEADER_H
