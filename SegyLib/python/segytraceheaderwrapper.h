#ifndef SEGYTRACEHEADERWRAPPER_H
#define SEGYTRACEHEADERWRAPPER_H

#include "segytraceheader.h"

#include <boost/python.hpp>

class SegyTraceHeaderWrapper{
public:
    SegyTraceHeaderWrapper(const SegyTraceHeader& sth):_sth(sth){}
    SegyTraceHeaderWrapper(){}
    int get(SegyTraceHeader::Field field){
        return _sth.get(field);
    }
    void set(SegyTraceHeader::Field field,int value){
        _sth.set(field,value);
    }

    static PyObject* getFieldList(){
        boost::python::list* l = new boost::python::list();
        for(auto field : SegyTraceHeader::fields){
            (*l).append(field);
        }
        return l->ptr();
    }

    static PyObject* getFieldDesc(SegyTraceHeader::Field field){
        boost::python::dict* l = new boost::python::dict();
        auto fieldInfo = SegyTraceHeader::getFieldInfo(field);
        (*l)["fieldName"] = fieldInfo.fieldName;
        (*l)["fullFieldName"] = fieldInfo.fullFieldName;
        (*l)["fieldDescription"] = fieldInfo.fieldDescription;

        return l->ptr();
    }

    PyObject* getDict(){
        boost::python::dict* l = new boost::python::dict();

        for(auto field: _sth.fields){
            (*l)[field] = _sth.get(field);
        }

        return l->ptr();
    }

    SegyTraceHeader _sth;

};


#endif // SEGYTRACEHEADERWRAPPER_H
