#define BOOST_PYTHON_STATIC_LIB
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include <string>

#include "segyheaderwrapper.h"
#include "segytraceheaderwrapper.h"
#include "sutraceheaderwrapper.h"
#include "segyfilewrapper.h"
#include "sufilewrapper.h"
#include "types.h"


using namespace boost::python;


BOOST_PYTHON_MODULE(Segy)
{
    enum_<Endian>("Endian").value("BIG_ENDIAN", Endian::ENDIAN_BIG)
            .value("LITTLE_ENDIAN", Endian::ENDIAN_LITTLE)
            .value("UNDEFINED_ENDIAN", Endian::ENDIAN_UNDEFINED);

    enum_<Format>("Format").value("IBM_FORMAT", Format::FORMAT_IBM)
            .value("INT4_FORMAT", Format::FORMAT_4INT)
            .value("INT2_FORMAT", Format::FORMAT_2INT)
            .value("FIXPOINT4_FORMAT", Format::FORMAT_4FIXEDPOINT)
            .value("IEEE4_FORMAT", Format::FORMAT_4IEEE)
            .value("INT1_FORMAT", Format::FORMAT_1INT)
            .value("IEEE8_FORMAT", Format::FORMAT_8IEEE);


    auto globalScope = scope();
    scope shScope = class_<SegyHeaderWrapper>("SegyHeader")
        .def("get",&SegyHeaderWrapper::get,args("name"))
        .def("set",&SegyHeaderWrapper::set,args("name","value"))
        .def("getFieldDesc",&SegyHeaderWrapper::getFieldDesc,args("name")).staticmethod("getFieldDesc")
        .def("getFieldList",&SegyHeaderWrapper::getFieldList).staticmethod("getFieldList")
        .def("to_dict",&SegyHeaderWrapper::getDict)
;

    enum_<SegyHeader::Field>("Field").value("jobId", SegyHeader::Field::jobId)
            .value("lineNumber", SegyHeader::Field::lineNumber)
            .value("reelNumber", SegyHeader::Field::reelNumber)
            .value("numTracesPerRecord", SegyHeader::Field::numTracesPerRecord)
            .value("numOfAxualiryTracesPerRecord", SegyHeader::Field::numOfAxualiryTracesPerRecord)
            .value("sampleInterval", SegyHeader::Field::sampleInterval)
            .value("sampleIntervalOrigin", SegyHeader::Field::sampleIntervalOrigin)
            .value("sampleCount", SegyHeader::Field::sampleCount)
            .value("sampleCountOrigin", SegyHeader::Field::sampleCountOrigin)
            .value("type", SegyHeader::Field::type)
            .value("cdpFold", SegyHeader::Field::cdpFold)
            .value("traceSorting", SegyHeader::Field::traceSorting)
            .value("verticalSum", SegyHeader::Field::verticalSum)
            .value("sweepFrStart", SegyHeader::Field::sweepFrStart)
            .value("sweepFrEnd", SegyHeader::Field::sweepFrEnd)
            .value("sweepLength", SegyHeader::Field::sweepLength)
            .value("sweepType", SegyHeader::Field::sweepType)
            .value("traceNumOfSweepChannel", SegyHeader::Field::traceNumOfSweepChannel)
            .value("sweepTaperLengthStart", SegyHeader::Field::sweepTaperLengthStart)
            .value("sweepTaperLengthEnd", SegyHeader::Field::sweepTaperLengthEnd)
            .value("taperType", SegyHeader::Field::taperType)
            .value("corrDataTraces", SegyHeader::Field::corrDataTraces)
            .value("binGainRecovery", SegyHeader::Field::binGainRecovery)
            .value("ampRecoveryMethod", SegyHeader::Field::ampRecoveryMethod)
            .value("measurementSys", SegyHeader::Field::measurementSys)
            .value("impSignal", SegyHeader::Field::impSignal)
            .value("polarityCode", SegyHeader::Field::polarityCode)
            .value("segyFormat", SegyHeader::Field::segyFormat)
            .value("fixedLength", SegyHeader::Field::fixedLength);

    scope gs2 = globalScope;
    scope sthScope = class_<SegyTraceHeaderWrapper>("SegyTraceHeader")
            .def("get",&SegyTraceHeaderWrapper::get,args("name"))
            .def("set",&SegyTraceHeaderWrapper::set,args("name","value"))
            .def("getFieldDesc",&SegyTraceHeaderWrapper::getFieldDesc,args("name")).staticmethod("getFieldDesc")
            .def("getFieldList",&SegyTraceHeaderWrapper::getFieldList).staticmethod("getFieldList")
            .def("to_dict",&SegyTraceHeaderWrapper::getDict)
    ;

    enum_<SegyTraceHeader::Field>("Field").value("traceNumberLine", SegyTraceHeader::Field::traceNumberLine)
            .value("traceNumberReel", SegyTraceHeader::Field::traceNumberReel)
            .value("originalFieldRecordNumber", SegyTraceHeader::Field::originalFieldRecordNumber)
            .value("traceSeqNumberFR", SegyTraceHeader::Field::traceSeqNumberFR)
            .value("sourceNumber", SegyTraceHeader::Field::sourceNumber)
            .value("cdpNumber", SegyTraceHeader::Field::cdpNumber)
            .value("traceSeqNumberCDP", SegyTraceHeader::Field::traceSeqNumberCDP)
            .value("traceIdCode", SegyTraceHeader::Field::traceIdCode)
            .value("numVerticalSumTraces", SegyTraceHeader::Field::numVerticalSumTraces)
            .value("numHorizontalStackedTraces", SegyTraceHeader::Field::numHorizontalStackedTraces)
            .value("dataUse", SegyTraceHeader::Field::dataUse)
            .value("distSrcRecv", SegyTraceHeader::Field::distSrcRecv)
            .value("recvElevation", SegyTraceHeader::Field::recvElevation)
            .value("surfElevation", SegyTraceHeader::Field::surfElevation)
            .value("depthBelowSurf", SegyTraceHeader::Field::depthBelowSurf)
            .value("datumElevationRecv", SegyTraceHeader::Field::datumElevationRecv)
            .value("datumElevationSrc", SegyTraceHeader::Field::datumElevationSrc)
            .value("waterDepthSrc", SegyTraceHeader::Field::waterDepthSrc)
            .value("waterDepthGroup", SegyTraceHeader::Field::waterDepthGroup)
            .value("elevScalar", SegyTraceHeader::Field::elevScalar)
            .value("coordScalar", SegyTraceHeader::Field::coordScalar)
            .value("srcCoordX", SegyTraceHeader::Field::srcCoordX)
            .value("srcCoordY", SegyTraceHeader::Field::srcCoordY)
            .value("recvGroupCoordX", SegyTraceHeader::Field::recvGroupCoordX)
            .value("recvGroupCoordY", SegyTraceHeader::Field::recvGroupCoordY)
            .value("coordUnit", SegyTraceHeader::Field::coordUnit)
            .value("weatheringVelocity", SegyTraceHeader::Field::weatheringVelocity)
            .value("subWeatheringVelocity", SegyTraceHeader::Field::subWeatheringVelocity)
            .value("upholeTimeSrc", SegyTraceHeader::Field::upholeTimeSrc)
            .value("upholeTimeRsvGroup", SegyTraceHeader::Field::upholeTimeRsvGroup)
            .value("srcStatCorrection", SegyTraceHeader::Field::srcStatCorrection)
            .value("rcvGroupStatCorrection", SegyTraceHeader::Field::rcvGroupStatCorrection)
            .value("totalStaticApplied", SegyTraceHeader::Field::totalStaticApplied)
            .value("lagTimeA", SegyTraceHeader::Field::lagTimeA)
            .value("lagTimeB", SegyTraceHeader::Field::lagTimeB)
            .value("delayRecTime", SegyTraceHeader::Field::delayRecTime)
            .value("muteTimeStart", SegyTraceHeader::Field::muteTimeStart)
            .value("muteTimeEnd", SegyTraceHeader::Field::muteTimeEnd)
            .value("sampleCount", SegyTraceHeader::Field::sampleCount)
            .value("sampleInterval", SegyTraceHeader::Field::sampleInterval)
            .value("gainType", SegyTraceHeader::Field::gainType)
            .value("instrGainCons", SegyTraceHeader::Field::instrGainCons)
            .value("instrInitGain", SegyTraceHeader::Field::instrInitGain)
            .value("correlated", SegyTraceHeader::Field::correlated)
            .value("sweepFrStart", SegyTraceHeader::Field::sweepFrStart)
            .value("sweepFrEnd", SegyTraceHeader::Field::sweepFrEnd)
            .value("sweepLength", SegyTraceHeader::Field::sweepLength)
            .value("sweepType", SegyTraceHeader::Field::sweepType)
            .value("sweepTraceTaperLengthStart", SegyTraceHeader::Field::sweepTraceTaperLengthStart)
            .value("sweepTraceTaperLengthEnd", SegyTraceHeader::Field::sweepTraceTaperLengthEnd)
            .value("taperType", SegyTraceHeader::Field::taperType)
            .value("aliasFilterFr", SegyTraceHeader::Field::aliasFilterFr)
            .value("aliasFilterSlope", SegyTraceHeader::Field::aliasFilterSlope)
            .value("notchFilterFr", SegyTraceHeader::Field::notchFilterFr)
            .value("notchFilterSlope", SegyTraceHeader::Field::notchFilterSlope)
            .value("lowCutFr", SegyTraceHeader::Field::lowCutFr)
            .value("highCutFr", SegyTraceHeader::Field::highCutFr)
            .value("lowCutSlope", SegyTraceHeader::Field::lowCutSlope)
            .value("highCutSlope", SegyTraceHeader::Field::highCutSlope)
            .value("year", SegyTraceHeader::Field::year)
            .value("dayOfYear", SegyTraceHeader::Field::dayOfYear)
            .value("hour", SegyTraceHeader::Field::hour)
            .value("min", SegyTraceHeader::Field::min)
            .value("sec", SegyTraceHeader::Field::sec)
            .value("timeCode", SegyTraceHeader::Field::timeCode)
            .value("traceWeightFactor", SegyTraceHeader::Field::traceWeightFactor)
            .value("geophoneRoll", SegyTraceHeader::Field::geophoneRoll)
            .value("geophoneFirstTrace", SegyTraceHeader::Field::geophoneFirstTrace)
            .value("geophoneLastTrace", SegyTraceHeader::Field::geophoneLastTrace)
            .value("gapSize", SegyTraceHeader::Field::gapSize)
            .value("travel", SegyTraceHeader::Field::travel)
            .value("cdpX", SegyTraceHeader::Field::cdpX)
            .value("cdpY", SegyTraceHeader::Field::cdpY)
            .value("inlineNumber", SegyTraceHeader::Field::inlineNumber)
            .value("xlineNumber", SegyTraceHeader::Field::xlineNumber)
            .value("shotpointNumber", SegyTraceHeader::Field::shotpointNumber)
            .value("shotpointScalar", SegyTraceHeader::Field::shotpointScalar)
            .value("traceMeasurementUnit", SegyTraceHeader::Field::traceMeasurementUnit)
            .value("transductionConstant", SegyTraceHeader::Field::transductionConstant)
            .value("transductionUnit", SegyTraceHeader::Field::transductionUnit)
            .value("devId", SegyTraceHeader::Field::devId)
            .value("timeScalar", SegyTraceHeader::Field::timeScalar)
            .value("srcType", SegyTraceHeader::Field::srcType)
            .value("srcEnergyDirection", SegyTraceHeader::Field::srcEnergyDirection)
            .value("srcMeasurement", SegyTraceHeader::Field::srcMeasurement)
            .value("srcMeasurementUnit", SegyTraceHeader::Field::srcMeasurementUnit)
    ;

    scope gs3 = globalScope;
    class_<SegyFileWrapper>("SegyFile")
        .def(init<String,String>(args("file","mode")))
        .def(init<String>(args("file")))
        .def("openFile", &SegyFileWrapper::openFile, args("file"))
        .def("closeFile", &SegyFileWrapper::closeFile)
        .def("isOpen", &SegyFileWrapper::isOpen)
        .def("getSampleCount", &SegyFileWrapper::getSampleCount)
        .def("setEbcidicHeader",&SegyFileWrapper::setEbcidicHeader,args("header"))
        .def("getEbcidicHeader",&SegyFileWrapper::getEbcidicHeader)
        .def("getTraceHeader", &SegyFileWrapper::getTraceHeader, args("traceNum"))
        .def("setTraceHeader", &SegyFileWrapper::setTraceHeader, args("traceNum","traceHeader"))
        .def("getTrace", &SegyFileWrapper::getTrace,args("traceNum"))
        .def("setTrace", &SegyFileWrapper::setTrace, args("traceNum","traceData"))
        .def("getTraceCount", &SegyFileWrapper::getTraceCount)
        .def("getHeader", &SegyFileWrapper::getHeader)
        .def("setHeader", &SegyFileWrapper::setHeader)
        .def("fflush",&SegyFileWrapper::fflush)
        .def("createSegy", &SegyFileWrapper::createSegy,return_value_policy<manage_new_object>()).staticmethod("createSegy")
    ;


}
