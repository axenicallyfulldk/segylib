#ifndef SEGYHEADERWRAPPER_H
#define SEGYHEADERWRAPPER_H

#include "segyheader.h"
#include <vector>
#include <string>
#include <boost/python.hpp>

class SegyHeaderWrapper{
public:
    SegyHeaderWrapper(const SegyHeader& sh):_sh(sh){}
    SegyHeaderWrapper(){}
    int get(SegyHeader::Field field){
        return _sh.get(field);
    }
    void set(SegyHeader::Field field,int value){
        _sh.set(field,value);
    }

    static PyObject* getFieldList(){
        boost::python::list* l = new boost::python::list();
        for(auto field : SegyHeader::fields){
            (*l).append(field);
        }
        return l->ptr();
    }

    static PyObject* getFieldDesc(SegyHeader::Field field){
        boost::python::dict* l = new boost::python::dict();
        auto fieldInfo = SegyHeader::getFieldInfo(field);
        (*l)["fieldName"] = fieldInfo.fieldName;
        (*l)["fullFieldName"] = fieldInfo.fullFieldName;
        (*l)["fieldDescription"] = fieldInfo.fieldDescription;

        return l->ptr();
    }

    PyObject* getDict(){
        boost::python::dict* l = new boost::python::dict();

        for(auto field: _sh.fields){
            (*l)[field] = _sh.get(field);
        }

        return l->ptr();
    }

    SegyHeader _sh;
};

#endif // SEGYHEADERWRAPPER_H
